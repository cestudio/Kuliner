package google.com.kuliner;

import android.app.Activity;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

public class Halaman_Utama_Inggris extends AppCompatActivity {

    public static Activity fa;
    public static Toolbar toolbar;

    PagerAdapterHalamanRekomendasiNoLogin adapternologin;
    PagerAdapterHalamanRekomendasiLogin adapterlogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_halaman__utama);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        fa = this;


        TabLayout tabLayout = (TabLayout) findViewById(R.id.sliding_tabs);
        if (getSharedPreferences("DATA",MODE_PRIVATE).getString("bahasa","").equals("1")) {
            tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.rekomen).setText("Rekomendasi"));
            tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.search).setText("Pencarian"));
            tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.kuliner).setText("Kuliner"));
            tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.user).setText("Akun"));
        }
        else{
            tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.rekomen).setText("Recommendation"));
            tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.search).setText("Search"));
            tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.kuliner).setText("Culinary"));
            tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.user).setText("Account"));
        }

        final ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        if (getSharedPreferences("DATA",MODE_PRIVATE).getString("statlogin","").equals("1")){
            adapterlogin = new PagerAdapterHalamanRekomendasiLogin
                    (getSupportFragmentManager(), tabLayout.getTabCount());
            viewPager.setAdapter(adapterlogin);
        }
        else{
            adapternologin = new PagerAdapterHalamanRekomendasiNoLogin
                    (getSupportFragmentManager(), tabLayout.getTabCount());
            viewPager.setAdapter(adapternologin);
        }

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }
}

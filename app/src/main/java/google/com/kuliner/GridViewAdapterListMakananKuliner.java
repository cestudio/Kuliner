package google.com.kuliner;

import android.app.Activity;
import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class GridViewAdapterListMakananKuliner extends ArrayAdapter<GridItemListMakananKuliner> {

    private Context mContext;
    private int layoutResourceId;
    private ArrayList<GridItemListMakananKuliner> mGridData = new ArrayList<GridItemListMakananKuliner>();

    public GridViewAdapterListMakananKuliner(Context mContext, int layoutResourceId, ArrayList<GridItemListMakananKuliner> mGridData) {
        super(mContext, layoutResourceId, mGridData);
        this.layoutResourceId = layoutResourceId;
        this.mContext = mContext;
        this.mGridData = mGridData;
    }


    /**
     * Updates grid data and refresh grid items.
     * @param mGridData
     */
    public void setGridData(ArrayList<GridItemListMakananKuliner> mGridData) {
        this.mGridData = mGridData;
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ViewHolder holder;

        if (row == null) {
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            holder = new ViewHolder();
            holder.idmakanan = (TextView) row.findViewById(R.id.idmakanan);
            holder.namamakanan = (TextView) row.findViewById(R.id.namamakanan);
            holder.gambar = (ImageView) row.findViewById(R.id.gambar);
            row.setTag(holder);
        } else {
            holder = (ViewHolder) row.getTag();
        }

        GridItemListMakananKuliner item = mGridData.get(position);
        holder.idmakanan.setText(Html.fromHtml(item.getIdmakanan()));
        holder.namamakanan.setText(Html.fromHtml(item.getNamamakanan()));

        Picasso.with(mContext).load(item.getGambar()).resize(500,500).into(holder.gambar);

        return row;
    }

    static class ViewHolder {
        TextView namamakanan,idmakanan;
        ImageView gambar;
    }
}
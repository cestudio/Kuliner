package google.com.kuliner;


public class GridItemListMakananKuliner {

    String gambar,namamakanan,idmakanan;

    public String getGambar() {
        return gambar;
    }

    public void setGambar(String gambar) {
        this.gambar = gambar;
    }

    public String getNamamakanan() {
        return namamakanan;
    }

    public void setNamamakanan(String namamakanan) {
        this.namamakanan = namamakanan;
    }

    public String getIdmakanan() {
        return idmakanan;
    }

    public void setIdmakanan(String idmakanan) {
        this.idmakanan = idmakanan;
    }
}
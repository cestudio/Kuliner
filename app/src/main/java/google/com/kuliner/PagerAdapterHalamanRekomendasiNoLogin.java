package google.com.kuliner;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;


public class PagerAdapterHalamanRekomendasiNoLogin extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public PagerAdapterHalamanRekomendasiNoLogin(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                HalamanRekomendasiFragment tab0 = new HalamanRekomendasiFragment();
                return tab0;
            case 1:
                CariHalamanKuliner tab1 = new CariHalamanKuliner();
                return tab1;
            case 2:
                HalamanKuliner tab2 = new HalamanKuliner();
                return tab2;
            case 3:
                AkunFragmentNoLogin tab3 = new AkunFragmentNoLogin();
                return tab3;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
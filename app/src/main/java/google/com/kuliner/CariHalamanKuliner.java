package google.com.kuliner;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.Image;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class CariHalamanKuliner extends Fragment {

    //Web api url
    public static String FEED_URL;

    private static final String TAG = HalamanRekomendasiFragment.class.getSimpleName();
    private GridView mGridView;
    private ProgressBar mProgressBar;
    private GridViewAdapter mGridAdapter;
    private ArrayList<GridItem> mGridData;

    Button cari;

    EditText etcari;

    TextView nodata;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.fragment_cari_halaman_kuliner, container, false);

        FEED_URL = "http://";
        FEED_URL += getActivity().getSharedPreferences("DATA",Context.MODE_PRIVATE).getString("IP","");
        FEED_URL += Config.HALAMANREKOMENDASI_URL;

        etcari = (EditText) rootview.findViewById(R.id.carihalamankulineredittextcari);
        nodata = (TextView) rootview.findViewById(R.id.nodata);
        cari = (Button) rootview.findViewById(R.id.carihalamankulinerbtncari);
        mGridView = (GridView) rootview.findViewById(R.id.gridView);
        mProgressBar = (ProgressBar) rootview.findViewById(R.id.progressBar);

        //Initialize with empty data
        mGridData = new ArrayList<>();
        mGridAdapter = new GridViewAdapter(getActivity(), R.layout.grid_item_layout, mGridData);
        mGridView.setAdapter(mGridAdapter);

        //Start download
        new AsyncHttpTask().execute(FEED_URL);
        mProgressBar.setVisibility(View.VISIBLE);

        mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {

                //Get item at position
                GridItem item = (GridItem) parent.getItemAtPosition(position);

                SharedPreferences.Editor editor = getActivity().getSharedPreferences("DATA", Context.MODE_PRIVATE).edit();
                editor.putString("detail", "rekomendasi");
                editor.putString("idkuliner", item.getIdkuliner());
                editor.putString("kuliner", item.getNamamakanan());
                editor.putString("idlokasi", item.getIdlokasi());
                editor.putString("lokasi", item.getNamalokasi());
                editor.putString("alamat", item.getAlamat());
                editor.putString("gambar", item.getGambarlokasi());
                editor.commit();

                //Pass the image title and url to DetailsActivity
                Intent intent = new Intent(getActivity(), DetailRekomendasi.class);
                startActivity(intent);
            }
        });

        if (getActivity().getSharedPreferences("DATA",Context.MODE_PRIVATE).getString("bahasa","").equals("2")){
            etcari.setHint("Search...");
            cari.setText("Search");
        }
        else{
            etcari.setHint("Cari...");
            cari.setText("Cari");
        }

        cari.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(etcari.getText().toString().replace(" ","%20").equals(" ")){
                    FEED_URL = "http://";
                    FEED_URL += getActivity().getSharedPreferences("DATA",Context.MODE_PRIVATE).getString("IP","");
                    FEED_URL += Config.HALAMANREKOMENDASI_URL;

                    //Initialize with empty data
                    mGridData = new ArrayList<>();
                    mGridAdapter = new GridViewAdapter(getActivity(), R.layout.grid_item_layout, mGridData);
                    mGridView.setAdapter(mGridAdapter);

                    //Start download
                    new AsyncHttpTask().execute(FEED_URL);
                    mProgressBar.setVisibility(View.VISIBLE);
                }
                else{
                    FEED_URL = "http://";
                    FEED_URL += getActivity().getSharedPreferences("DATA",Context.MODE_PRIVATE).getString("IP","");
                    FEED_URL += Config.CARIHALAMANREKOMENDASI_URL;
                    FEED_URL += "?namakuliner="+etcari.getText().toString().replace(" ","%20");

                    //Initialize with empty data
                    mGridData = new ArrayList<>();
                    mGridAdapter = new GridViewAdapter(getActivity(), R.layout.grid_item_layout, mGridData);
                    mGridView.setAdapter(mGridAdapter);

                    //Start download
                    new AsyncHttpTask().execute(FEED_URL);
                    mProgressBar.setVisibility(View.VISIBLE);
                }
            }
        });

        return rootview;
    }

    //Downloading data asynchronously
    public class AsyncHttpTask extends AsyncTask<String, Void, Integer> {

        @Override
        protected Integer doInBackground(String... params) {
            Integer result = 0;
            try {
                // Create Apache HttpClient
                HttpClient httpclient = new DefaultHttpClient();
                HttpResponse httpResponse = httpclient.execute(new HttpGet(params[0]));
                int statusCode = httpResponse.getStatusLine().getStatusCode();

                // 200 represents HTTP OK
                if (statusCode == 200) {
                    String response = streamToString(httpResponse.getEntity().getContent());
                    parseResult(response);
                    result = 1; // Successful
                } else {
                    result = 0; //"Failed
                }
            } catch (Exception e) {
                Log.d(TAG, e.getLocalizedMessage());
            }
            return result;
        }

        @Override
        protected void onPostExecute(Integer result) {
            // Download complete. Let us update UI
            if (result == 1) {
                mGridAdapter.setGridData(mGridData);

            } else {
//                Toast.makeText(getActivity(), "Failed to fetch data!", Toast.LENGTH_SHORT).show();
            }
            mProgressBar.setVisibility(View.GONE);
        }
    }

    String streamToString(InputStream stream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(stream));
        String line;
        String result = "";
        while ((line = bufferedReader.readLine()) != null) {
            result += line;
        }

        // Close stream
        if (null != stream) {
            stream.close();
        }
        return result;
    }

    /**
     * Parsing the feed results and get the list
     * @param result
     */
    private void parseResult(String result) {
        try {
            JSONObject response = new JSONObject(result);
            JSONArray posts = response.optJSONArray("result");
            GridItem item;
            if (posts.length()==0){
                nodata.setVisibility(View.VISIBLE);
                mGridView.setVisibility(View.GONE);
            }
            else{
                nodata.setVisibility(View.GONE);
                mGridView.setVisibility(View.VISIBLE);
            }
            for (int i = 0; i < posts.length(); i++) {
                JSONObject post = posts.optJSONObject(i);
                String idlokasi = post.optString("idlokasi");
                String idkuliner = post.optString("idkuliner");
                String namalokasi = post.optString("namalokasi");
                String namamakanan = post.optString("namamakanan");
                String alamat = post.optString("alamat");
                String gambarkuliner = post.optString("gambarkuliner");
                String rating = post.optString("rating");
                item = new GridItem();
                item.setIdlokasi(idlokasi);
                item.setIdkuliner(idkuliner);
                item.setKuliner(namalokasi);
                item.setAlamat(alamat);
                item.setNamamakanan(namamakanan);
                item.setNamalokasi(namalokasi);
                item.setRating(rating);
                item.setGambarkuliner(gambarkuliner);
                if (null != posts && posts.length() > 0) {
                    JSONObject attachment = posts.getJSONObject(i);
                    if (attachment != null)
                        item.setGambarlokasi(attachment.getString("gambarlokasi"));
                }

                mGridData.add(item);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}

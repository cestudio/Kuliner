package google.com.kuliner;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class TambahLokasi extends ActionBarActivity implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    String[] makananid;
    String[] makanannama;

    public static String FEED_URL;

    private GoogleMap mMap;
    GoogleApiClient mGoogleApiClient;
    Location mLastLocation;
    Marker mCurrLocationMarker;
    LocationRequest mLocationRequest;

    private static final String TAG = TambahLokasi.class.getSimpleName();

    EditText etnamalokasi,etalamat,etjambuka,etjamtutup,etkontak,etcatatan;

    Spinner etmenu;

    boolean klikmapstatus=false;

    Handler handler = new Handler();
    Runnable runnable = new Runnable() {
        public void run() {
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                checkLocationPermission();
                isStoragePermissionGranted();
            }
            handler.postDelayed(runnable, 1000);
        }
    };

    Button kirim,batal;
    private Button btCamera;

    static boolean a=false;

    private ImageView imageView;

    private Uri fileUri;

    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;

    public static final int MEDIA_TYPE_IMAGE = 1;

    private String filePath = null;

    TextView txtPercentage;

    long totalSize = 0;

    private ProgressBar progressBar;

    Spinner spinkecamatan,spinkelurahan;

    ArrayAdapter<String> adapter;
    List<String> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getSharedPreferences("DATA",Context.MODE_PRIVATE).getString("bahasa","").equals("2")){
            setContentView(R.layout.activity_tambah_lokasi_inggris);
        }
        else {
            setContentView(R.layout.activity_tambah_lokasi);
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        etnamalokasi = (EditText) findViewById(R.id.tambahlokasinamalokasi);
//        etalamat = (EditText) findViewById(R.id.tambahlokasialamat);
        etjambuka = (EditText) findViewById(R.id.tambahlokasijambuka);
        etjamtutup = (EditText) findViewById(R.id.tambahlokasijamtutup);
        etkontak = (EditText) findViewById(R.id.tambahlokasikontak);
        etcatatan = (EditText) findViewById(R.id.tambahlokasicatatan);
        btCamera = (Button) findViewById(R.id.btntambahlokasifoto);
        kirim = (Button) findViewById(R.id.btntambahlokasikirim);
        batal = (Button) findViewById(R.id.btntambahlokasibatal);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        txtPercentage = (TextView) findViewById(R.id.txtPercentage);

        spinkecamatan = (Spinner) findViewById(R.id.kecamatan);
        spinkelurahan = (Spinner) findViewById(R.id.kelurahan);

        imageView = (ImageView) findViewById(R.id.tambahlokasifoto);

        imageView.setVisibility(View.GONE);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.kecamatan, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinkecamatan.setAdapter(adapter);

        spinkecamatan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(spinkecamatan.getSelectedItem().equals("Bangorejo")){
                    ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(TambahLokasi.this,
                            R.array.kecamatanBangorejo, android.R.layout.simple_spinner_item);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    spinkelurahan.setAdapter(adapter);
                }
                else if(spinkecamatan.getSelectedItem().equals("Banyuwangi Kota")){
                    ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(TambahLokasi.this,
                            R.array.kecamatanBanyuwangi, android.R.layout.simple_spinner_item);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    spinkelurahan.setAdapter(adapter);
                }
                else if(spinkecamatan.getSelectedItem().equals("Cluring")){
                    ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(TambahLokasi.this,
                            R.array.kecamatanCluring, android.R.layout.simple_spinner_item);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    spinkelurahan.setAdapter(adapter);
                }
                else if(spinkecamatan.getSelectedItem().equals("Gambiran")){
                    ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(TambahLokasi.this,
                            R.array.kecamatanGambiran, android.R.layout.simple_spinner_item);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    spinkelurahan.setAdapter(adapter);
                }
                else if(spinkecamatan.getSelectedItem().equals("Genteng")){
                    ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(TambahLokasi.this,
                            R.array.kecamatanGenteng, android.R.layout.simple_spinner_item);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    spinkelurahan.setAdapter(adapter);
                }
                else if(spinkecamatan.getSelectedItem().equals("Giri")){
                    ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(TambahLokasi.this,
                            R.array.kecamatanGenteng, android.R.layout.simple_spinner_item);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    spinkelurahan.setAdapter(adapter);
                }
                else if(spinkecamatan.getSelectedItem().equals("Glagah")){
                    ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(TambahLokasi.this,
                            R.array.kecamatanGlagah, android.R.layout.simple_spinner_item);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    spinkelurahan.setAdapter(adapter);
                }
                else if(spinkecamatan.getSelectedItem().equals("Glenmore")){
                    ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(TambahLokasi.this,
                            R.array.kecamatanGlenmore, android.R.layout.simple_spinner_item);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    spinkelurahan.setAdapter(adapter);
                }
                else if(spinkecamatan.getSelectedItem().equals("Kabat")){
                    ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(TambahLokasi.this,
                            R.array.kecamatanKabat, android.R.layout.simple_spinner_item);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    spinkelurahan.setAdapter(adapter);
                }
                else if(spinkecamatan.getSelectedItem().equals("Kalibaru")){
                    ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(TambahLokasi.this,
                            R.array.kecamatanKalibaru, android.R.layout.simple_spinner_item);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    spinkelurahan.setAdapter(adapter);
                }
                else if(spinkecamatan.getSelectedItem().equals("Kalipuro")){
                    ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(TambahLokasi.this,
                            R.array.kecamatanKalipuro, android.R.layout.simple_spinner_item);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    spinkelurahan.setAdapter(adapter);
                }
                else if(spinkecamatan.getSelectedItem().equals("Licin")){
                    ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(TambahLokasi.this,
                            R.array.kecamatanLicin, android.R.layout.simple_spinner_item);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    spinkelurahan.setAdapter(adapter);
                }
                else if(spinkecamatan.getSelectedItem().equals("Muncar")){
                    ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(TambahLokasi.this,
                            R.array.kecamatanMuncar, android.R.layout.simple_spinner_item);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    spinkelurahan.setAdapter(adapter);
                }
                else if(spinkecamatan.getSelectedItem().equals("Pesanggaran")){
                    ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(TambahLokasi.this,
                            R.array.kecamatanPesanggaran, android.R.layout.simple_spinner_item);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    spinkelurahan.setAdapter(adapter);
                }
                else if(spinkecamatan.getSelectedItem().equals("Purwoharjo")){
                    ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(TambahLokasi.this,
                            R.array.kecamatanPurwoharjo, android.R.layout.simple_spinner_item);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    spinkelurahan.setAdapter(adapter);
                }
                else if(spinkecamatan.getSelectedItem().equals("Rogojampi")){
                    ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(TambahLokasi.this,
                            R.array.kecamatanRogojampi, android.R.layout.simple_spinner_item);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    spinkelurahan.setAdapter(adapter);
                }
                else if(spinkecamatan.getSelectedItem().equals("Sempu")){
                    ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(TambahLokasi.this,
                            R.array.kecamatanSempu, android.R.layout.simple_spinner_item);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    spinkelurahan.setAdapter(adapter);
                }
                else if(spinkecamatan.getSelectedItem().equals("Siliragung")){
                    ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(TambahLokasi.this,
                            R.array.kecamatanSiliragung, android.R.layout.simple_spinner_item);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    spinkelurahan.setAdapter(adapter);
                }
                else if(spinkecamatan.getSelectedItem().equals("Singojuruh")){
                    ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(TambahLokasi.this,
                            R.array.kecamatanSingojuruh, android.R.layout.simple_spinner_item);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    spinkelurahan.setAdapter(adapter);
                }
                else if(spinkecamatan.getSelectedItem().equals("Songgon")){
                    ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(TambahLokasi.this,
                            R.array.kecamatanSonggon, android.R.layout.simple_spinner_item);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    spinkelurahan.setAdapter(adapter);
                }
                else if(spinkecamatan.getSelectedItem().equals("Srono")){
                    ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(TambahLokasi.this,
                            R.array.kecamatanSrono, android.R.layout.simple_spinner_item);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    spinkelurahan.setAdapter(adapter);
                }
                else if(spinkecamatan.getSelectedItem().equals("Tegaldlimo")){
                    ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(TambahLokasi.this,
                            R.array.kecamatanTegaldlimo, android.R.layout.simple_spinner_item);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    spinkelurahan.setAdapter(adapter);
                }
                else if(spinkecamatan.getSelectedItem().equals("Tegalsari")){
                    ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(TambahLokasi.this,
                            R.array.kecamatanTegalsari, android.R.layout.simple_spinner_item);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    spinkelurahan.setAdapter(adapter);
                }
                else if(spinkecamatan.getSelectedItem().equals("Wongsorejo")){
                    ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(TambahLokasi.this,
                            R.array.kecamatanWongsorejo, android.R.layout.simple_spinner_item);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    spinkelurahan.setAdapter(adapter);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        etjambuka.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(TambahLokasi.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        if (selectedHour<10&&selectedMinute<10){
                            if (selectedHour<12) {
                                etjambuka.setText("0" + selectedHour + ".0" + selectedMinute);
                            }
                            else{
                                etjambuka.setText("0" + selectedHour + ".0" + selectedMinute);
                            }
                        }
                        else if (selectedHour<10){
                            if (selectedHour<12) {
                                etjambuka.setText( "0" + selectedHour + "." + selectedMinute);
                            }
                            else{
                                etjambuka.setText( "0" + selectedHour + "." + selectedMinute);
                            }
                        }
                        else if (selectedMinute<10){
                            if (selectedHour<12) {
                                etjambuka.setText(selectedHour + ".0" + selectedMinute);
                            }
                            else{
                                etjambuka.setText(selectedHour + ".0" + selectedMinute);
                            }
                        }
                        else{
                            if (selectedHour<12) {
                                etjambuka.setText(selectedHour + "." + selectedMinute);
                            }
                            else{
                                etjambuka.setText(selectedHour + "." + selectedMinute);
                            }
                        }
                    }
                }, hour, minute, true);//Yes 24 hour time
                if (getSharedPreferences("DATA",Context.MODE_PRIVATE).getString("bahasa","").equals("2")){
                    mTimePicker.setTitle("Select Open");
                }
                else {
                    mTimePicker.setTitle("Pilih Jam Buka");
                }

                mTimePicker.show();

            }
        });

        etjamtutup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(TambahLokasi.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        if (selectedHour<10&&selectedMinute<10){
                            if (selectedHour<12) {
                                etjamtutup.setText("0" + selectedHour + ".0" + selectedMinute);
                            }
                            else{
                                etjamtutup.setText("0" + selectedHour + ".0" + selectedMinute);
                            }
                        }
                        else if (selectedHour<10){
                            if (selectedHour<12) {
                                etjamtutup.setText( "0" + selectedHour + "." + selectedMinute);
                            }
                            else{
                                etjamtutup.setText( "0" + selectedHour + "." + selectedMinute);
                            }
                        }
                        else if (selectedMinute<10){
                            if (selectedHour<12) {
                                etjamtutup.setText(selectedHour + ".0" + selectedMinute);
                            }
                            else{
                                etjamtutup.setText(selectedHour + ".0" + selectedMinute);
                            }
                        }
                        else{
                            if (selectedHour<12) {
                                etjamtutup.setText(selectedHour + "." + selectedMinute);
                            }
                            else{
                                etjamtutup.setText(selectedHour + "." + selectedMinute);
                            }
                        }
                    }
                }, hour, minute, true);//Yes 24 hour time
                if (getSharedPreferences("DATA",Context.MODE_PRIVATE).getString("bahasa","").equals("2")){
                    mTimePicker.setTitle("Select Open");
                }
                else {
                    mTimePicker.setTitle("Pilih Jam Buka");
                }

                mTimePicker.show();
            }
        });

        kirim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (etnamalokasi.getText().toString().equals("")) {
                    Toast.makeText(TambahLokasi.this, "Data tidak boleh kosong!", Toast.LENGTH_SHORT).show();
                }
                else{
//                    if (etalamat.getText().toString().equals("")) {
//                        Toast.makeText(TambahLokasi.this, "Data tidak boleh kosong!", Toast.LENGTH_SHORT).show();
//                    }
//                    else{
                        if (klikmapstatus==false) {
                            Toast.makeText(TambahLokasi.this, "Pilih koordinat!", Toast.LENGTH_SHORT).show();
                        }
                        else{
                            if (etjambuka.getText().toString().equals("")) {
                                Toast.makeText(TambahLokasi.this, "Data tidak boleh kosong!", Toast.LENGTH_SHORT).show();
                            }
                            else{
                                if (etkontak.getText().toString().equals("")) {
                                    Toast.makeText(TambahLokasi.this, "Data tidak boleh kosong!", Toast.LENGTH_SHORT).show();
                                }
                                else{
                                    if (etcatatan.getText().toString().equals("")) {
                                        Toast.makeText(TambahLokasi.this, "Data tidak boleh kosong!", Toast.LENGTH_SHORT).show();
                                    }
                                    else{
                                        new UploadFileToServer().execute();
                                    }
                                }
                            }
//                        }
                    }
                }
            }
        });

        btCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                captureImage();
            }
        });

        batal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        runnable.run();
    }

    private void captureImage() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);

        filePath = fileUri.getPath();

        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

        startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);


    }

    /**
     * Receiving activity result method will be called after closing the camera
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {

                if (filePath != null) {
                    previewMedia(true);
                } else {
                    Toast.makeText(getApplicationContext(),
                            "Sorry, file path is missing!", Toast.LENGTH_LONG).show();
                }


            } else if (resultCode == RESULT_CANCELED) {

                Toast.makeText(getApplicationContext(),
                        "User cancelled image capture", Toast.LENGTH_SHORT)
                        .show();

            } else {
                Toast.makeText(getApplicationContext(),
                        "Sorry! Failed to capture image", Toast.LENGTH_SHORT)
                        .show();
            }
        }
    }

    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    /**
     * returning image / video
     */
    private static File getOutputMediaFile(int type) {

        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                "Download");

        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(TAG, "Oops! Failed create "
                        + "Download" + " directory");
                return null;
            }
        }

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } else {
            return null;
        }

        return mediaFile;
    }

    /**
     * Displaying captured image/video on the screen
     * */
    private void previewMedia(boolean isImage) {
        if (isImage) {
            imageView.setVisibility(View.VISIBLE);
            BitmapFactory.Options options = new BitmapFactory.Options();

            options.inSampleSize = 8;

            final Bitmap bitmap = BitmapFactory.decodeFile(filePath, options);

            Bitmap.createScaledBitmap(bitmap,250,250, true);

            imageView.setImageBitmap(bitmap);

            imageView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {

            @Override
            public void onMapClick(LatLng latLng) {

                klikmapstatus=true;

                MarkerOptions markerOptions = new MarkerOptions();

                markerOptions.position(latLng);


                SharedPreferences.Editor editor = getSharedPreferences("DATA", MODE_PRIVATE).edit();
                editor.putString("latitude", String.valueOf(latLng.latitude));
                editor.putString("longitude", String.valueOf(latLng.longitude));
                editor.commit();

                if (getSharedPreferences("DATA", Context.MODE_PRIVATE).getString("bahasa","").equals("2")){
                    markerOptions.title("New Location");
                }
                else{
                    markerOptions.title("Lokasi Baru");
                }


                mMap.clear();

                mMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));

                mMap.addMarker(markerOptions);
            }
        });

        //Initialize Google Play Services
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    android.Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                mMap.setMyLocationEnabled(true);
            }
        }
        else {
            buildGoogleApiClient();
            mMap.setMyLocationEnabled(true);
        }
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(Bundle bundle) {

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(5000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        if (ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, TambahLokasi.this);
        }

    }

    public void onConnectionSuspended(int i) {

    }

    public void onLocationChanged(Location location) {

        mLastLocation = location;
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }

        //Place current location marker
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());

        //move map camera
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(15));


        //stop location updates
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }

    }

    public void onConnectionFailed(ConnectionResult connectionResult) {

    }
    public  boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG,"Permission is granted");
                return true;
            } else {

                Log.v(TAG,"Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            Log.v(TAG,"Permission is granted");
            return true;
        }
    }
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    public boolean checkLocationPermission(){
        if (ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    android.Manifest.permission.ACCESS_FINE_LOCATION)) {


                //Prompt the user once explanation has been shown
                ActivityCompat.requestPermissions(this,new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},MY_PERMISSIONS_REQUEST_LOCATION);

            } else {
                ActivityCompat.requestPermissions(this,new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }


    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    if (ContextCompat.checkSelfPermission(this,
                            android.Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        if (mGoogleApiClient == null) {
                            buildGoogleApiClient();
                        }
                        mMap.setMyLocationEnabled(true);
                    }

                } else {

                }
                return;
            }

        }
    }

    private class UploadFileToServer extends AsyncTask<Void, Integer, String> {
        @Override
        protected void onPreExecute() {
            progressBar.setProgress(0);
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            progressBar.setVisibility(View.VISIBLE);

            progressBar.setProgress(progress[0]);

            txtPercentage.setText(String.valueOf(progress[0]) + "%");
        }

        @Override
        protected String doInBackground(Void... params) {
            return uploadFile();
        }

        @SuppressWarnings("deprecation")
        private String uploadFile() {
            String responseString = null;

            try {

                FEED_URL = "http://";
                FEED_URL += getSharedPreferences("DATA",MODE_PRIVATE).getString("IP","");
                FEED_URL += Config.TAMBAHLOKASI_URL;
                FEED_URL += "?namalokasi="+etnamalokasi.getText().toString().replace(" ","%20");
                FEED_URL += "&" + URLEncoder.encode("alamat", "UTF-8") + "=" + URLEncoder.encode(spinkecamatan.getSelectedItem().toString()+", "+spinkelurahan.getSelectedItem().toString(), "UTF-8");
//                FEED_URL += "&" + URLEncoder.encode("alamat", "UTF-8") + "=" + URLEncoder.encode(etalamat.getText().toString(), "UTF-8");
                FEED_URL += "&jambuka="+(etjambuka.getText().toString()+" - "+etjamtutup.getText().toString()).replace(" ","%20");
                FEED_URL += "&kontak="+etkontak.getText().toString().replace(" ","%20");
                FEED_URL += "&" + URLEncoder.encode("catatan", "UTF-8") + "=" + URLEncoder.encode(etcatatan.getText().toString(), "UTF-8");
                FEED_URL += "&latitude="+getSharedPreferences("DATA",MODE_PRIVATE).getString("latitude","").replace(" ","%20");
                FEED_URL += "&longitude="+getSharedPreferences("DATA",MODE_PRIVATE).getString("longitude","").replace(" ","%20");
                FEED_URL += "&iduser="+getSharedPreferences("DATA",MODE_PRIVATE).getString("iduser","").replace(" ","%20");

            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(FEED_URL);


                AndroidMultiPartEntity entity = new AndroidMultiPartEntity(
                        new AndroidMultiPartEntity.ProgressListener() {

                            @Override
                            public void transferred(long num) {
                                publishProgress((int) ((num / (float) totalSize) * 100));
                            }
                        });

                File sourceFile = new File(fileUri.getPath());

                entity.addPart("image", new FileBody(sourceFile));

                totalSize = entity.getContentLength();
                httppost.setEntity(entity);

                HttpResponse response = httpclient.execute(httppost);
                HttpEntity r_entity = response.getEntity();

                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode == 200) {
                    responseString = EntityUtils.toString(r_entity);
                } else {
                    responseString = "Error occurred! Http Status Code: "
                            + statusCode;
                }

            } catch (ClientProtocolException e) {
                responseString = e.toString();
            } catch (IOException e) {
                responseString = e.toString();
            }

            return responseString;

        }

        @Override
        protected void onPostExecute(String result) {
            Log.e(TAG, "Response from server: " + result);
            showAlert(result);
            super.onPostExecute(result);
        }

    }

    private void showAlert(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(message).setTitle("Proses upload selesai")
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finish();
                    }
                })
//                .setNegativeButton("TAMBAH LAGI", new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int id) {
//                        startActivity(new Intent(TambahLokasi.this,TambahLokasi.class));
//                        finish();
//                    }
//                })
        ;
        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        fileUri = savedInstanceState.getParcelable("file_uri");
    }
}

package google.com.kuliner;

import android.app.Activity;
import android.app.Dialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.NotificationCompat;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.SecureRandom;
import java.util.Random;


public class ResetPassword extends Dialog {
    public Activity c;
    public Button ubah;
    public EditText etEmail;

    public static String FEED_URL;

    private static final String TAG_RESULT = "result";
    private static final String TAG_UBAHPASSWORD = "ubah";

    static boolean a=false;

    JSONArray rs = null;

    char[] CHARSET_AZ_09 = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".toCharArray();

    String password;

    public ResetPassword(Activity a) {
        super(a);
        // TODO Auto-generated constructor stub
        this.c = a;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.layoutubahpassword);
        etEmail = (EditText) findViewById(R.id.editTextEmail);
        ubah = (Button) findViewById(R.id.btnubah);

        ubah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(etEmail.getText().toString().equals("")){
                    Toast.makeText(getContext(), "Email Tidak Boleh Kosong", Toast.LENGTH_SHORT).show();
                }
                else{
                    SharedPreferences.Editor editor = c.getSharedPreferences("DATA", Context.MODE_PRIVATE).edit();
                    editor.putString("email", etEmail.getText().toString());
                    editor.commit();
                    new setPassword().execute();
                }
            }
        });
    }

    public static String randomString(char[] characterSet, int length) {
        Random random = new SecureRandom();
        char[] result = new char[length];
        for (int i = 0; i < result.length; i++) {
            int randomCharIndex = random.nextInt(characterSet.length);
            result[i] = characterSet[randomCharIndex];
        }
        return new String(result);
    }

    private void sendNotificationSound(String message) {
        Intent intent = new Intent(getContext(), UbahPassword.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(getContext(),0 /* request code */, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        long[] pattern = {500,500,500,500,500};

        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder notificationBuilder = (NotificationCompat.Builder) new NotificationCompat.Builder(getContext())
                .setSmallIcon(R.drawable.ic_logo)
                .setContentTitle("Notifikasi Lupa Password")
                .setContentText(message)
                .setAutoCancel(true)
                .setVibrate(pattern)
                .setLights(Color.BLUE,1,1)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager = (NotificationManager) getContext().getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());


    }

    private class setPassword extends AsyncTask<String, String, JSONObject> {
        @Override
        protected void onPreExecute(){

        }
        @Override
        protected JSONObject doInBackground(String... args)
        {
            password = randomString(CHARSET_AZ_09,4);

            FEED_URL = "http://";
            FEED_URL += c.getSharedPreferences("DATA",Context.MODE_PRIVATE).getString("IP","");
            FEED_URL += Config.UBAHPASSWORD_URL;
            FEED_URL += "?email="+c.getSharedPreferences("DATA",Context.MODE_PRIVATE).getString("email","");
            FEED_URL += "&password="+password;
            JSONParser jParser = new JSONParser();
            JSONObject json = jParser.getJSONFromUrl(FEED_URL);

            Log.e("Reset Password = ",FEED_URL);

            if(json==null)
            {
                a=false;
            }
            else a=true;
            return json;
        }
        protected void onPostExecute(JSONObject json) {
            if(a==true)
            {
                try{
                    Log.e("status",a+"");
                    rs = json.getJSONArray(TAG_RESULT);
                    for(int i=0; i<rs.length();i++)
                    {
                        JSONObject a = rs.getJSONObject(i);
                        String ubah = a.getString(TAG_UBAHPASSWORD);
                        if (ubah.equals("0")){
                            Toast.makeText(getContext(), "Update data gagal!", Toast.LENGTH_SHORT).show();
                        }
                        else if (ubah.equals("1")){
                            sendNotificationSound("Password baru anda adalah "+password);
                            Toast.makeText(getContext(), "Data sudah diperbarui!", Toast.LENGTH_SHORT).show();
                            dismiss();
                        }
                    }
                }catch(JSONException e)
                {
                    e.printStackTrace();
                }
            }
            else {

                Toast.makeText(getContext(), "Server Maintenance", Toast.LENGTH_SHORT).show();
                Log.e("status",a+"");}
        }

    }
}

package google.com.kuliner;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Register extends AppCompatActivity {

    TextView nama,email,password,konfirmasipassword;

    public static String FEED_URL;

    Button register;

    private static final String TAG_RESULT = "result";
    private static final String TAG_PENDAFTARAN = "pendaftaran";

    static boolean a=false;

    JSONArray rs = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        nama = (TextView) findViewById(R.id.registernamalengkap);
        email = (TextView) findViewById(R.id.registeremail);
        password = (TextView) findViewById(R.id.registerpassword);
        konfirmasipassword = (TextView) findViewById(R.id.registerkonfirmasipassword);

        register = (Button) findViewById(R.id.btnregister);

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new SendRegister().execute();

            }
        });
    }

    private class SendRegister extends AsyncTask<String, String, JSONObject> {
        @Override
        protected void onPreExecute(){

        }
        //menjalankan proses di background, tidak mengganggu proses lain
        @Override
        protected JSONObject doInBackground(String... args)
        {
            FEED_URL = "http://";
            FEED_URL += getSharedPreferences("DATA",MODE_PRIVATE).getString("IP","");
            FEED_URL += Config.REGISTER_URL;
            String URL = FEED_URL;
            URL += "?nama="+nama.getText().toString().replace(" ","%20");
            URL += "&email="+email.getText().toString();
            URL += "&password="+password.getText().toString();


            //Membuat JSON Parser instance
            JSONParser jParser = new JSONParser();

            //mengambil JSON String dari url
            JSONObject json = jParser.getJSONFromUrl(URL);
            if(json==null)
            {
                a=false;
            }
            else a=true;
            return json;
        }
        protected void onPostExecute(JSONObject json) {
            if(a==true)
            {
                try{
                    Log.e("status",a+"");
                    //mengambil array rumah sakit
                    //mengambil array rumah sakit
                    rs = json.getJSONArray(TAG_RESULT);
                    //loop pada rumah sakit
                    for(int i=0; i<rs.length();i++)
                    {
                        JSONObject a = rs.getJSONObject(i);
                        String pendaftaran = a.getString(TAG_PENDAFTARAN);
                        if (pendaftaran.equals("0")){
                            Toast.makeText(Register.this, "Pendaftaran tidak berhasil, coba lagi!", Toast.LENGTH_SHORT).show();
                        }
                        else if (pendaftaran.equals("1")){
                            Toast.makeText(Register.this, "Email anda sudah terdaftar!", Toast.LENGTH_SHORT).show();
                        }
                        else{
                            Toast.makeText(Register.this, "Pendaftaran berhasil!", Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(Register.this,Login.class));
                            finish();
                        }

                    }
                }catch(JSONException e)
                {
                    e.printStackTrace();
                }
            }
            //Jika tidak ada koneksi atau server down, keluarkan message error
            else {

                Toast.makeText(getApplicationContext(), "Server Maintenance", Toast.LENGTH_SHORT).show();
                Log.e("status",a+"");}
        }

    }
}

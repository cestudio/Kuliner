package google.com.kuliner;


public class Config {

    public static String REGISTER_URL="/kuliner/register.php";
    public static String LOGIN_URL="/kuliner/login.php";
    public static String DETAIL_URL="/kuliner/detailhalamanrekomendasi.php";
    public static String DETAILHALAMANKULINER_URL="/kuliner/detailhalamankuliner.php";
    public static String HALAMANREKOMENDASI_URL="/kuliner/halamanrekomendasi.php";
    public static String LISTMAKANANHALAMANREKOMENDASI_URL="/kuliner/listmakananhalamanrekomendasi.php";
    public static String HALAMANKULINER_URL="/kuliner/halamankuliner.php";
    public static String LISTMAKANAN="/kuliner/listmakanan.php";
    public static String CARIHALAMANREKOMENDASI_URL="/kuliner/carihalamankuliner.php";
    public static String LISTKOMETARDETAILHALAMANREKOMENDASI_URL="/kuliner/listkomentardetailhalamanrekomendasi.php";
    public static String KIRIMRATING_URL="/kuliner/kirimrating.php";
    public static String TAMBAHLOKASI_URL="/kuliner/tambahlokasi.php";
    public static String TAMBAHKULINER_URL="/kuliner/tambahkuliner.php";
    public static String MAPKULINER_URL="/kuliner/mapkuliner.php";
    public static String PETAKULINER_URL="/kuliner/petakuliner.php";
    public static String AKUN_URL="/kuliner/akun.php";
    public static String UPDATEAKUN_URL="/kuliner/updateakun.php";
    public static String UBAHPASSWORD_URL="/kuliner/ubahpassword.php";
    public static String LISTLOKASI_URL="/kuliner/listlokasi.php";

}

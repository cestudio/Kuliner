package google.com.kuliner;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;


public class CustomSpinnerAdapter extends BaseAdapter {
    Context context;
    String[] namamakanan;
    String[] makananid;
    LayoutInflater inflter;

    public CustomSpinnerAdapter(Context applicationContext, String[] makananid, String[] namamakanan) {
        this.context = applicationContext;
        this.makananid = makananid;
        this.namamakanan = namamakanan;
        inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return namamakanan.length;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.custom_spinner_items, null);
        TextView idmakanan = (TextView) view.findViewById(R.id.idmakanan);
        TextView names = (TextView) view.findViewById(R.id.namamakanan);
        idmakanan.setText(makananid[i]);
        names.setText(namamakanan[i]);
        return view;
    }
}
package google.com.kuliner;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.Image;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.NotificationCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.SecureRandom;
import java.util.Random;

public class Login extends AppCompatActivity {

    public static String FEED_URL;

    ImageView logo;

    TextView buatakun,lupapassword;

    Button signin;

    EditText email,password;

    private static final String TAG_RESULT = "result";
    private static final String TAG_IDUSER = "iduser";
    private static final String TAG_UBAHPASSWORD = "ubahpassword";

    static boolean a=false;

    JSONArray rs = null;

    StringBuilder randomStringBuilder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        logo = (ImageView) findViewById(R.id.logo);
        email = (EditText) findViewById(R.id.loginemail);
        password = (EditText) findViewById(R.id.loginpassword);
        signin = (Button) findViewById(R.id.btnlogin);
        buatakun = (TextView) findViewById(R.id.loginbuatakun);
        lupapassword = (TextView) findViewById(R.id.loginlupapassword);

        signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new SendLogin().execute();
            }
        });

        buatakun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Login.this,Register.class));
                finish();
            }
        });

        logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IP ip = new IP(Login.this);
                ip.show();
            }
        });

        lupapassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ResetPassword rp = new ResetPassword(Login.this);
                rp.show();
            }

        });

    }

    //JSON parser, mengambil data dari web service,
    private class SendLogin extends AsyncTask<String, String, JSONObject> {
        @Override
        protected void onPreExecute(){

        }
        //menjalankan proses di background, tidak mengganggu proses lain
        @Override
        protected JSONObject doInBackground(String... args)
        {
            FEED_URL = "http://";
            FEED_URL += getSharedPreferences("DATA",MODE_PRIVATE).getString("IP","");
            FEED_URL += Config.LOGIN_URL;
            String URL = FEED_URL;
            URL += "?email="+email.getText().toString();
            URL += "&password="+password.getText().toString();


            //Membuat JSON Parser instance
            JSONParser jParser = new JSONParser();

            //mengambil JSON String dari url
            JSONObject json = jParser.getJSONFromUrl(URL);
            if(json==null)
            {
                a=false;
            }
            else a=true;
            return json;
        }
        protected void onPostExecute(JSONObject json) {
            if(a==true)
            {
                try{
                    Log.e("status",a+"");
                    //mengambil array rumah sakit
                    //mengambil array rumah sakit
                    rs = json.getJSONArray(TAG_RESULT);
                    //loop pada rumah sakit
                    for(int i=0; i<rs.length();i++)
                    {
                        JSONObject a = rs.getJSONObject(i);
                        String iduser = a.getString(TAG_IDUSER);
                        if (iduser.equals("0")){
                            Toast.makeText(Login.this, "Email/Password Anda Salah!", Toast.LENGTH_SHORT).show();
                        }
                        else{
                            SharedPreferences.Editor editor = getSharedPreferences("DATA", MODE_PRIVATE).edit();
                            editor.putString("statlogin", "1");
                            editor.putString("bahasa", "1");
                            editor.putString("iduser", iduser);
                            editor.putString("password", password.getText().toString());
                            editor.commit();
                            Halaman_Utama.fa.finish();
                            startActivity(new Intent(Login.this,Halaman_Utama.class));
                            finish();
                        }
                        
                    }
                }catch(JSONException e)
                {
                    e.printStackTrace();
                }
            }
            //Jika tidak ada koneksi atau server down, keluarkan message error
            else {

                Toast.makeText(getApplicationContext(), "Server Maintenance", Toast.LENGTH_SHORT).show();
                Log.e("status",a+"");}
        }

    }
}

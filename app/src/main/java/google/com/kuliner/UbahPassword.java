package google.com.kuliner;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class UbahPassword extends AppCompatActivity {

    public static String FEED_URL;

    EditText etpassword;

    Button ubahpassword;

    private static final String TAG_RESULT = "result";
    private static final String TAG_UBAHPASSWORD = "ubah";

    static boolean a=false;

    JSONArray rs = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ubah_password);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        etpassword = (EditText) findViewById(R.id.etpasswordbaru);
        ubahpassword = (Button) findViewById(R.id.btnpasswordbaru);

        ubahpassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new setPassword().execute();
            }
        });
    }

    private class setPassword extends AsyncTask<String, String, JSONObject> {
        @Override
        protected void onPreExecute(){

        }
        //menjalankan proses di background, tidak mengganggu proses lain
        @Override
        protected JSONObject doInBackground(String... args)
        {
            FEED_URL = "http://";
            FEED_URL += getSharedPreferences("DATA",MODE_PRIVATE).getString("IP","");
            FEED_URL += Config.UBAHPASSWORD_URL;
            String URL = FEED_URL;
            URL += "?email="+getSharedPreferences("DATA",MODE_PRIVATE).getString("email","");
            URL += "&password="+etpassword.getText().toString().replace(" ","%20");

            //Membuat JSON Parser instance
            JSONParser jParser = new JSONParser();

            //mengambil JSON String dari url
            JSONObject json = jParser.getJSONFromUrl(URL);
            if(json==null)
            {
                a=false;
            }
            else a=true;
            return json;
        }
        protected void onPostExecute(JSONObject json) {
            if(a==true)
            {
                try{
                    Log.e("status",a+"");
                    rs = json.getJSONArray(TAG_RESULT);
                    for(int i=0; i<rs.length();i++)
                    {
                        JSONObject a = rs.getJSONObject(i);
                        String ubah = a.getString(TAG_UBAHPASSWORD);
                        if (ubah.equals("0")){
                            Toast.makeText(UbahPassword.this, "Update data gagal!", Toast.LENGTH_SHORT).show();
                        }
                        else if (ubah.equals("1")){
                            Toast.makeText(UbahPassword.this, "Data sudah diperbarui!", Toast.LENGTH_SHORT).show();

                        }
                    }
                }catch(JSONException e)
                {
                    e.printStackTrace();
                }
            }
            //Jika tidak ada koneksi atau server down, keluarkan message error
            else {

                Toast.makeText(getApplicationContext(), "Server Maintenance", Toast.LENGTH_SHORT).show();
                Log.e("status",a+"");}
        }

    }
}

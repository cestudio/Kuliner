package google.com.kuliner;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.SimpleAdapter;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;

public class DetailRekomendasiKuliner extends ActionBarActivity {

    public static String FEED_URL;

    TextView namalokasi,namakuliner,alamatkuliner,deskripsi,rating;
    ImageView imggambar,bintang1,bintang2,bintang3,bintang4,bintang5;

    public static Activity fa;

    String id;

    private static final String TAG_RESULT = "result";
    private static final String TAG_GAMBARKULINER = "gambar";
    private static final String TAG_NAMAMAKANAN = "namamakanan";
    private static final String TAG_NAMALOKASI = "namalokasi";
    private static final String TAG_ALAMATLOKASI = "alamatlokasi";
    private static final String TAG_DISKRIPSI= "deskripsi";
    private static final String TAG_RATING = "rating";

    static boolean a=false;

    JSONArray rs = null;

    JSONArray data = null;

    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_rekomendasi_kuliner);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        fa=this;;

        namakuliner = (TextView) findViewById(R.id.detailnamakuliner);
        namalokasi = (TextView) findViewById(R.id.detailnamalokasi);
        alamatkuliner = (TextView) findViewById(R.id.detailalamatkuliner);
        deskripsi = (TextView) findViewById(R.id.detaildeskripsi);
        rating = (TextView) findViewById(R.id.detailnilairating);
        imggambar = (ImageView) findViewById(R.id.detailgambar);
        bintang1 = (ImageView) findViewById(R.id.detailbintang1);
        bintang2 = (ImageView) findViewById(R.id.detailbintang2);
        bintang3 = (ImageView) findViewById(R.id.detailbintang3);
        bintang4 = (ImageView) findViewById(R.id.detailbintang4);
        bintang5 = (ImageView) findViewById(R.id.detailbintang5);

        new Detail().execute();
    }

    private class Detail extends AsyncTask<String, String, JSONObject> {
        @Override
        protected void onPreExecute(){

        }
        @Override
        protected JSONObject doInBackground(String... args)
        {
            FEED_URL = "http://";
            FEED_URL += getSharedPreferences("DATA",MODE_PRIVATE).getString("IP","");
            FEED_URL += Config.DETAILHALAMANKULINER_URL;
            FEED_URL += "?idmakanan="+getSharedPreferences("DATA",MODE_PRIVATE).getString("idkuliner","");
            String URL = FEED_URL;

            Log.e("URL : ",URL);

            JSONParser jParser = new JSONParser();

            JSONObject json = jParser.getJSONFromUrl(URL);
            if(json==null)
            {
                a=false;
            }
            else a=true;
            return json;
        }
        protected void onPostExecute(JSONObject json) {
            if(a==true)
            {
                try{
                    Log.e("status",a+"");
                    rs = json.getJSONArray(TAG_RESULT);
                    for(int i=0; i<rs.length();i++)
                    {
                        JSONObject a = rs.getJSONObject(i);
                        String strgambarkuliner = a.getString(TAG_GAMBARKULINER);
                        String strnamalokasi = a.getString(TAG_NAMALOKASI);
                        String strnamamakanan = a.getString(TAG_NAMAMAKANAN);
                        String stralamatlokasi = a.getString(TAG_ALAMATLOKASI);
                        String strdiskripsi = a.getString(TAG_DISKRIPSI);
                        String strrating = a.getString(TAG_RATING);

                        namalokasi.setText(strnamalokasi);
                        namakuliner.setText(strnamamakanan);
                        alamatkuliner.setText(stralamatlokasi);
                        deskripsi.setText(strdiskripsi);
                        rating.setText(strrating);

                        Picasso.with(DetailRekomendasiKuliner.this).load(strgambarkuliner).resize(500,500).into(imggambar);

                        if (Float.valueOf(strrating)==0.0){
                            bintang1.setBackgroundResource(R.drawable.nostar);
                            bintang2.setBackgroundResource(R.drawable.nostar);
                            bintang3.setBackgroundResource(R.drawable.nostar);
                            bintang4.setBackgroundResource(R.drawable.nostar);
                            bintang5.setBackgroundResource(R.drawable.nostar);
                        }
                        else if (Float.valueOf(strrating)<=0.5){
                            bintang1.setBackgroundResource(R.drawable.halfstar);
                            bintang2.setBackgroundResource(R.drawable.nostar);
                            bintang3.setBackgroundResource(R.drawable.nostar);
                            bintang4.setBackgroundResource(R.drawable.nostar);
                            bintang5.setBackgroundResource(R.drawable.nostar);
                        }
                        else if (Float.valueOf(strrating)<=1.0){
                            bintang1.setBackgroundResource(R.drawable.fullstar);
                            bintang2.setBackgroundResource(R.drawable.nostar);
                            bintang3.setBackgroundResource(R.drawable.nostar);
                            bintang4.setBackgroundResource(R.drawable.nostar);
                            bintang5.setBackgroundResource(R.drawable.nostar);
                        }
                        else if (Float.valueOf(strrating)<=1.5){
                            bintang1.setBackgroundResource(R.drawable.fullstar);
                            bintang2.setBackgroundResource(R.drawable.halfstar);
                            bintang3.setBackgroundResource(R.drawable.nostar);
                            bintang4.setBackgroundResource(R.drawable.nostar);
                            bintang5.setBackgroundResource(R.drawable.nostar);
                        }
                        else if (Float.valueOf(strrating)<=2.0){
                            bintang1.setBackgroundResource(R.drawable.fullstar);
                            bintang2.setBackgroundResource(R.drawable.fullstar);
                            bintang3.setBackgroundResource(R.drawable.nostar);
                            bintang4.setBackgroundResource(R.drawable.nostar);
                            bintang5.setBackgroundResource(R.drawable.nostar);
                        }
                        else if (Float.valueOf(strrating)<=2.5){
                            bintang1.setBackgroundResource(R.drawable.fullstar);
                            bintang2.setBackgroundResource(R.drawable.fullstar);
                            bintang3.setBackgroundResource(R.drawable.halfstar);
                            bintang4.setBackgroundResource(R.drawable.nostar);
                            bintang5.setBackgroundResource(R.drawable.nostar);
                        }
                        else if (Float.valueOf(strrating)<=3.0){
                            bintang1.setBackgroundResource(R.drawable.fullstar);
                            bintang2.setBackgroundResource(R.drawable.fullstar);
                            bintang3.setBackgroundResource(R.drawable.fullstar);
                            bintang4.setBackgroundResource(R.drawable.nostar);
                            bintang5.setBackgroundResource(R.drawable.nostar);
                        }
                        else if (Float.valueOf(strrating)<=3.5){
                            bintang1.setBackgroundResource(R.drawable.fullstar);
                            bintang2.setBackgroundResource(R.drawable.fullstar);
                            bintang3.setBackgroundResource(R.drawable.fullstar);
                            bintang4.setBackgroundResource(R.drawable.halfstar);
                            bintang5.setBackgroundResource(R.drawable.nostar);
                        }
                        else if (Float.valueOf(strrating)<=4.0){
                            bintang1.setBackgroundResource(R.drawable.fullstar);
                            bintang2.setBackgroundResource(R.drawable.fullstar);
                            bintang3.setBackgroundResource(R.drawable.fullstar);
                            bintang4.setBackgroundResource(R.drawable.fullstar);
                            bintang5.setBackgroundResource(R.drawable.nostar);
                        }
                        else if (Float.valueOf(strrating)<=4.5){
                            bintang1.setBackgroundResource(R.drawable.fullstar);
                            bintang2.setBackgroundResource(R.drawable.fullstar);
                            bintang3.setBackgroundResource(R.drawable.fullstar);
                            bintang4.setBackgroundResource(R.drawable.fullstar);
                            bintang5.setBackgroundResource(R.drawable.halfstar);
                        }
                        else if (Float.valueOf(strrating)<=5.0){
                            bintang1.setBackgroundResource(R.drawable.fullstar);
                            bintang2.setBackgroundResource(R.drawable.fullstar);
                            bintang3.setBackgroundResource(R.drawable.fullstar);
                            bintang4.setBackgroundResource(R.drawable.fullstar);
                            bintang5.setBackgroundResource(R.drawable.fullstar);
                        }
                    }
                }catch(JSONException e)
                {
                    e.printStackTrace();
                }
            }
            else {

                Toast.makeText(getApplicationContext(), "Server Maintenance", Toast.LENGTH_SHORT).show();
                Log.e("status",a+"");}
        }

    }

}
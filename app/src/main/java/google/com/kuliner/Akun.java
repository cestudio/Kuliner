package google.com.kuliner;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.vision.text.Line;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Akun extends AppCompatActivity {

    public static String FEED_URL;

    LinearLayout akun;

    TextView namapengguna,emailpengguna,jumlahpost,jumlahkomentar,aquitance;
    EditText etnamapengguna,etemailpengguna;

    Button ubahakun,simpan;

    private static final String TAG_RESULT = "result";
    private static final String TAG_NAMAPENGGUNA = "namapengguna";
    private static final String TAG_EMAILPENGGUNA = "emailpengguna";
    private static final String TAG_DATAPOST = "datapost";
    private static final String TAG_DATAKOMENTAR = "datakomentar";
    private static final String TAG_UPDATE = "update";

    static boolean a=false;

    JSONArray rs = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_akun);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        akun = (LinearLayout) findViewById(R.id.layoutakun);
        etnamapengguna = (EditText) findViewById(R.id.etakunnamapengguna);
        namapengguna = (TextView) findViewById(R.id.akunnamapengguna);
        etemailpengguna = (EditText) findViewById(R.id.etakunemailpengguna);
        emailpengguna = (TextView) findViewById(R.id.akunemailpengguna);
        jumlahpost = (TextView) findViewById(R.id.akundatapost);
        jumlahkomentar = (TextView) findViewById(R.id.akundatakomentar);
        aquitance = (TextView) findViewById(R.id.akunaquitance);
        simpan = (Button) findViewById(R.id.btnakunsimpan);
        ubahakun = (Button) findViewById(R.id.btnakunubahakun);

        if (getSharedPreferences("DATA", Context.MODE_PRIVATE).getString("bahasa","").equals("2")){
            simpan.setText("Save");
            ubahakun.setText("Change Account");
        }
        else {
            simpan.setText("Simpan");
            ubahakun.setText("Ubah Akun");
        }

        new getAkun().execute();

        ubahakun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etnamapengguna.setVisibility(View.VISIBLE);
                namapengguna.setVisibility(View.GONE);
                etemailpengguna.setVisibility(View.VISIBLE);
                emailpengguna.setVisibility(View.GONE);
                simpan.setVisibility(View.VISIBLE);
                ubahakun.setVisibility(View.GONE);

            }
        });

        simpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(etnamapengguna.getText().toString().equals(" ")){
                    Toast.makeText(Akun.this, "Nama pengguna tidak boleh kosong!", Toast.LENGTH_SHORT).show();
                }
                else{
                    if(etemailpengguna.getText().toString().equals(" ")){
                        Toast.makeText(Akun.this, "Email tidak boleh kosong!", Toast.LENGTH_SHORT).show();
                    }
                    else{
                        etnamapengguna.setVisibility(View.GONE);
                        namapengguna.setVisibility(View.VISIBLE);
                        etemailpengguna.setVisibility(View.GONE);
                        emailpengguna.setVisibility(View.VISIBLE);
                        simpan.setVisibility(View.GONE);
                        ubahakun.setVisibility(View.VISIBLE);
                        new setAkun().execute();
                    }
                }
            }
        });
    }

    //JSON parser, mengambil data dari web service,
    private class getAkun extends AsyncTask<String, String, JSONObject> {
        @Override
        protected void onPreExecute(){

        }
        //menjalankan proses di background, tidak mengganggu proses lain
        @Override
        protected JSONObject doInBackground(String... args)
        {
            FEED_URL = "http://";
            FEED_URL += getSharedPreferences("DATA",MODE_PRIVATE).getString("IP","");
            FEED_URL += Config.AKUN_URL;
            String URL = FEED_URL;
            URL += "?iduser="+getSharedPreferences("DATA",MODE_PRIVATE).getString("iduser","");

            Log.e("Update =",FEED_URL);

            //Membuat JSON Parser instance
            JSONParser jParser = new JSONParser();

            //mengambil JSON String dari url
            JSONObject json = jParser.getJSONFromUrl(URL);
            if(json==null)
            {
                a=false;
            }
            else a=true;
            return json;
        }
        protected void onPostExecute(JSONObject json) {
            if(a==true)
            {
                try{
                    Log.e("status",a+"");
                    //mengambil array rumah sakit
                    //mengambil array rumah sakit
                    rs = json.getJSONArray(TAG_RESULT);
                    //loop pada rumah sakit
                    for(int i=0; i<rs.length();i++)
                    {
                        JSONObject a = rs.getJSONObject(i);
                        String strnamapengguna = a.getString(TAG_NAMAPENGGUNA);
                        String stremailpengguna = a.getString(TAG_EMAILPENGGUNA);
                        String strdatapost = a.getString(TAG_DATAPOST);
                        String strdatakomentar = a.getString(TAG_DATAKOMENTAR);

                        etnamapengguna.setText(strnamapengguna);
                        namapengguna.setText(strnamapengguna);
                        etemailpengguna.setText(stremailpengguna);
                        emailpengguna.setText(stremailpengguna);
                        if (getSharedPreferences("DATA", Context.MODE_PRIVATE).getString("bahasa","").equals("2")){
                            jumlahpost.setText(strdatapost+"\nPost");
                            jumlahkomentar.setText(strdatakomentar+"\nComment");
                        }
                        else {
                            jumlahpost.setText(strdatapost+"\nPostingan");
                            jumlahkomentar.setText(strdatakomentar+"\nKomentar");
                        }

                        if (Integer.parseInt(strdatapost)<=10){
                            if (getSharedPreferences("DATA", Context.MODE_PRIVATE).getString("bahasa","").equals("2")){
                                aquitance.setText("Amateur");
                            }
                            else {
                                aquitance.setText("Amatir");
                            }
                        }
                        else if (Integer.parseInt(strdatapost)<=30){
                            if (getSharedPreferences("DATA", Context.MODE_PRIVATE).getString("bahasa","").equals("2")){
                                aquitance.setText("Beginner");
                            }
                            else {
                                aquitance.setText("Pemula");
                            }
                        }
                        else if (Integer.parseInt(strdatapost)<=50){
                            if (getSharedPreferences("DATA", Context.MODE_PRIVATE).getString("bahasa","").equals("2")){
                                aquitance.setText("Professional");
                            }
                            else {
                                aquitance.setText("Profesional");
                            }
                        }
                        else if (Integer.parseInt(strdatapost)>50){
                            if (getSharedPreferences("DATA", Context.MODE_PRIVATE).getString("bahasa","").equals("2")){
                                aquitance.setText("Master");
                            }
                            else {
                                aquitance.setText("Master");
                            }
                        }
                    }
                }catch(JSONException e)
                {
                    e.printStackTrace();
                }
            }
            //Jika tidak ada koneksi atau server down, keluarkan message error
            else {

                Toast.makeText(getApplicationContext(), "Server Maintenance", Toast.LENGTH_SHORT).show();
                Log.e("status",a+"");}
        }

    }

    //JSON parser, mengambil data dari web service,
    private class setAkun extends AsyncTask<String, String, JSONObject> {
        @Override
        protected void onPreExecute(){

        }
        //menjalankan proses di background, tidak mengganggu proses lain
        @Override
        protected JSONObject doInBackground(String... args)
        {
            FEED_URL = "http://";
            FEED_URL += getSharedPreferences("DATA",MODE_PRIVATE).getString("IP","");
            FEED_URL += Config.UPDATEAKUN_URL;
            String URL = FEED_URL;
            URL += "?iduser="+getSharedPreferences("DATA",MODE_PRIVATE).getString("iduser","");
            URL += "&namapengguna="+etnamapengguna.getText().toString().replace(" ","%20");
            URL += "&emailpengguna="+etemailpengguna.getText().toString().replace(" ","%20");


            //Membuat JSON Parser instance
            JSONParser jParser = new JSONParser();

            //mengambil JSON String dari url
            JSONObject json = jParser.getJSONFromUrl(URL);
            if(json==null)
            {
                a=false;
            }
            else a=true;
            return json;
        }
        protected void onPostExecute(JSONObject json) {
            if(a==true)
            {
                try{
                    Log.e("status",a+"");
                    rs = json.getJSONArray(TAG_RESULT);
                    for(int i=0; i<rs.length();i++)
                    {
                        JSONObject a = rs.getJSONObject(i);
                        String update = a.getString(TAG_UPDATE);
                        if (update.equals("0")){
                            Toast.makeText(Akun.this, "Update data gagal!", Toast.LENGTH_SHORT).show();
                        }
                        else if (update.equals("1")){
                            Toast.makeText(Akun.this, "Data sudah diperbarui!", Toast.LENGTH_SHORT).show();
                            namapengguna.setText(etnamapengguna.getText().toString().replace(" ","%20"));
                            emailpengguna.setText(etemailpengguna.getText().toString().replace(" ","%20"));
                        }
                    }
                }catch(JSONException e)
                {
                    e.printStackTrace();
                }
            }
            //Jika tidak ada koneksi atau server down, keluarkan message error
            else {

                Toast.makeText(getApplicationContext(), "Server Maintenance", Toast.LENGTH_SHORT).show();
                Log.e("status",a+"");}
        }

    }
}

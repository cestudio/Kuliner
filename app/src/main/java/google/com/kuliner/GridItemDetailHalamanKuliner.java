package google.com.kuliner;


public class GridItemDetailHalamanKuliner {

    String idmakanan,namamakanan,deskripsimakanan,gambar,rating;

    public String getNamamakanan() {
        return namamakanan;
    }

    public void setNamamakanan(String namamakanan) {
        this.namamakanan = namamakanan;
    }

    public String getIdmakanan() {
        return idmakanan;
    }

    public void setIdmakanan(String idmakanan) {
        this.idmakanan = idmakanan;
    }

    public String getDeskripsimakanan() {
        return deskripsimakanan;
    }

    public void setDeskripsimakanan(String deskripsimakanan) {
        this.deskripsimakanan = deskripsimakanan;
    }

    public String getGambar() {
        return gambar;
    }

    public void setGambar(String gambar) {
        this.gambar = gambar;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }
}
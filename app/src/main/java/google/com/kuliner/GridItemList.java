package google.com.kuliner;


public class GridItemList {

    String gambar,kuliner,rating,id,alamat;

    public String getGambar() {

        return gambar;

    }

    public void setGambar(String gambar) {

        this.gambar = gambar;

    }

    public String getKuliner() {

        return kuliner;

    }

    public void setKuliner(String kuliner) {

        this.kuliner = kuliner;

    }

    public String getRating() {

        return rating;

    }

    public void setRating(String rating) {

        this.rating = rating;

    }

    public String getId() {

        return id;

    }

    public void setId(String id) {

        this.id = id;

    }

    public String getAlamat() {

        return alamat;

    }

    public void setAlamat(String alamat) {

        this.alamat = alamat;

    }
}
package google.com.kuliner;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableRow;

public class AkunFragmentLogin extends Fragment {

    TableRow akunbtneditakun,akunbtntambahlokasi,akunbtntambahkuliner,akunbtntentang,akunbtnbantuan,akunbtnlogout;

    View rootview;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (getActivity().getSharedPreferences("DATA",Context.MODE_PRIVATE).getString("bahasa","").equals("2")){
            rootview = inflater.inflate(R.layout.fragment_akun_fragment_login_inggris, container, false);
        }
        else{
            rootview = inflater.inflate(R.layout.fragment_akun_fragment_login, container, false);
        }


        akunbtneditakun = (TableRow) rootview.findViewById(R.id.akunbtneditakun);
        akunbtntambahlokasi = (TableRow) rootview.findViewById(R.id.akunbtntambahlokasi);
        akunbtntambahkuliner = (TableRow) rootview.findViewById(R.id.akunbtntambahkuliner);
        akunbtntentang = (TableRow) rootview.findViewById(R.id.akunbtntentang);
        akunbtnlogout = (TableRow) rootview.findViewById(R.id.akunbtnlogout);

        akunbtneditakun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(),Akun.class));
            }
        });

        akunbtnlogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = getActivity().getSharedPreferences("DATA", Context.MODE_PRIVATE).edit();
                editor.putString("statlogin", "0");
                editor.commit();
                Halaman_Utama.fa.finish();
                startActivity(getActivity().getIntent());
            }
        });

        akunbtntambahlokasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(),TambahLokasi.class));
            }
        });

        akunbtntambahkuliner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(),TambahKuliner.class));
            }
        });

        akunbtntentang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(),Tentang.class));
            }
        });

        return rootview;
    }
}

package google.com.kuliner;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;


public class Komentar extends Dialog {
    public Activity c;
    public Button kirim;
    public EditText etKomentar;

    public Komentar(Activity a) {
        super(a);
        // TODO Auto-generated constructor stub
        this.c = a;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.layoutkomentar);
        etKomentar = (EditText) findViewById(R.id.editTextKomentar);
        kirim = (Button) findViewById(R.id.btnkirim);

        kirim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                KirimRating();
            }
        });
    }

    public void KirimRating(){
        class GetDataJSON extends AsyncTask<String, Void, String> {

            @Override
            protected String doInBackground(String... params) {
                try {

                    String link;
                    link = "http://";
                    link += c.getSharedPreferences("DATA",Context.MODE_PRIVATE).getString("IP","");
                    link += Config.KIRIMRATING_URL;
                    link += "?idmakanan="+c.getSharedPreferences("DATA",Context.MODE_PRIVATE).getString("idkuliner","");
                    link += "&idlokasi="+c.getSharedPreferences("DATA",Context.MODE_PRIVATE).getString("idlokasi","");
                    link += "&iduser="+c.getSharedPreferences("DATA",Context.MODE_PRIVATE).getString("iduser","");
                    link += "&komentar="+etKomentar.getText().toString().replace(" ","%20");
                    link += "&nilairating="+c.getSharedPreferences("DATA",Context.MODE_PRIVATE).getString("nilairating","");

                    Log.e("Komentar = ",link);

                    URL url = new URL(link);
                    URLConnection conn = url.openConnection();

                    conn.setDoOutput(true);
                    OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());

                    wr.flush();

                    BufferedReader reader = new BufferedReader(new
                            InputStreamReader(conn.getInputStream()));

                    StringBuilder sb = new StringBuilder();
                    String line = null;

                    // Read Server Response
                    while ((line = reader.readLine()) != null) {
                        sb.append(line);
                        break;
                    }

                    return sb.toString();
                } catch (Exception e) {
                    return new String("Exception: " + e.getMessage());
                }
            }
            @Override
            protected void onPostExecute (String result){
                if (result.trim().equals("Sukses mengirim komentar")){
                    DetailRekomendasi.fa.finish();
                    ListMakanan.fa.finish();
                    DetailRekomendasi.fa.finish();
                    dismiss();
                }
                else{

                }
            }
        }
        GetDataJSON g = new GetDataJSON();
        g.execute();
    }
}

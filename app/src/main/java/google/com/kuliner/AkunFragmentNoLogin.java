package google.com.kuliner;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableRow;

public class AkunFragmentNoLogin extends Fragment {

    TableRow akunbtnlogin,akunbtntentang,akunbtnbantuan;

    View rootview;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (getActivity().getSharedPreferences("DATA", Context.MODE_PRIVATE).getString("bahasa","").equals("2")){
            rootview = inflater.inflate(R.layout.fragment_akun_non_login_inggris, container, false);
        }
        else{
            rootview = inflater.inflate(R.layout.fragment_akun_non_login, container, false);
        }

        akunbtnlogin = (TableRow) rootview.findViewById(R.id.akunbtnlogin);
        akunbtntentang = (TableRow) rootview.findViewById(R.id.akunbtntentang);

        akunbtnlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(),Login.class));

            }
        });

        akunbtntentang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(),Tentang.class));
            }
        });

        return rootview;
    }
}

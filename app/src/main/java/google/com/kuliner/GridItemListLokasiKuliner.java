package google.com.kuliner;


public class GridItemListLokasiKuliner {

    String gambar,namalokasi,idlokasi;

    public String getGambar() {

        return gambar;

    }

    public void setGambar(String gambar) {

        this.gambar = gambar;

    }

    public String getNamalokasi() {

        return namalokasi;

    }

    public void setNamalokasi(String namalokasi) {

        this.namalokasi = namalokasi;

    }

    public String getIdlokasi() {

        return idlokasi;

    }

    public void setIdlokasi(String idlokasi) {

        this.idlokasi = idlokasi;

    }

}
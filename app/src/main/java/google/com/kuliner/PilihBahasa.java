package google.com.kuliner;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class PilihBahasa extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pilih_bahasa);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Button bahasaindonesia = (Button) findViewById(R.id.pilihbahasaindonesia);
        Button bahasainggris = (Button) findViewById(R.id.pilihbahasainggris);

        ImageView ip = (ImageView) findViewById(R.id.ip);

        bahasaindonesia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = getSharedPreferences("DATA", MODE_PRIVATE).edit();
                editor.putString("bahasa", "1");
                editor.commit();
                startActivity(new Intent(PilihBahasa.this,Halaman_Utama.class));
                finish();
            }
        });

        bahasainggris.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = getSharedPreferences("DATA", MODE_PRIVATE).edit();
                editor.putString("bahasa", "2");
                editor.commit();
                startActivity(new Intent(PilihBahasa.this,Halaman_Utama_Inggris.class));
                finish();
            }
        });

        ip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IP ip = new IP(PilihBahasa.this);
                ip.show();
            }
        });
    }

}

package google.com.kuliner;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class Halaman_Utama extends AppCompatActivity {

    public static Activity fa;
    public static Toolbar toolbar;

    PagerAdapterHalamanRekomendasiNoLogin adapternologin;
    PagerAdapterHalamanRekomendasiLogin adapterlogin;

    TabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_halaman__utama);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        fa = this;

//        ImageView bahasaindonesia = (ImageView) findViewById(R.id.indonesia);
//        ImageView bahasainggris = (ImageView) findViewById(R.id.inggris);

        tabLayout = (TabLayout) findViewById(R.id.sliding_tabs);
        if (getSharedPreferences("DATA",MODE_PRIVATE).getString("bahasa","").equals("1")) {
            toolbar.setTitle("Halaman Utama");

            tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.rekomen).setText("Rekomendasi"));
            tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.search).setText("Pencarian"));
            tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.kuliner).setText("Kuliner"));
            tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.user).setText("Akun"));
        }
        else{
            toolbar.setTitle("Dashboard");

            tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.rekomen).setText("Recommendation"));
            tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.search).setText("Search"));
            tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.kuliner).setText("Culinary"));
            tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.user).setText("Account"));
        }
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        final ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        if (getSharedPreferences("DATA",MODE_PRIVATE).getString("statlogin","").equals("1")){
            adapterlogin = new PagerAdapterHalamanRekomendasiLogin
                    (getSupportFragmentManager(), tabLayout.getTabCount());
            viewPager.setAdapter(adapterlogin);
        }
        else{
            adapternologin = new PagerAdapterHalamanRekomendasiNoLogin
                    (getSupportFragmentManager(), tabLayout.getTabCount());
            viewPager.setAdapter(adapternologin);
        }

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected
                    (TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

//        bahasaindonesia.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                SharedPreferences.Editor editor = getSharedPreferences("DATA", MODE_PRIVATE).edit();
//                editor.putString("bahasa", "1");
//                editor.commit();
//
//                toolbar = (Toolbar) findViewById(R.id.toolbar);
//                setSupportActionBar(toolbar);
//                toolbar.setTitle("Halaman Utama");
//
//                tabLayout.removeAllTabs();
//
//                tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.rekomen).setText("Rekomendasi"));
//                tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.search).setText("Pencarian"));
//                tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.kuliner).setText("Kuliner"));
//                tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.user).setText("Akun"));
//
//                tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
//
//                final ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
//                if (getSharedPreferences("DATA",MODE_PRIVATE).getString("statlogin","").equals("1")){
//                    adapterlogin = new PagerAdapterHalamanRekomendasiLogin
//                            (getSupportFragmentManager(), tabLayout.getTabCount());
//                    viewPager.setAdapter(adapterlogin);
//                }
//                else{
//                    adapternologin = new PagerAdapterHalamanRekomendasiNoLogin
//                            (getSupportFragmentManager(), tabLayout.getTabCount());
//                    viewPager.setAdapter(adapternologin);
//                }
//
//                viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
//                tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
//                    @Override
//                    public void onTabSelected(TabLayout.Tab tab) {
//                        viewPager.setCurrentItem(tab.getPosition());
//                    }
//
//                    @Override
//                    public void onTabUnselected
//                            (TabLayout.Tab tab) {
//
//                    }
//
//                    @Override
//                    public void onTabReselected(TabLayout.Tab tab) {
//
//                    }
//                });
//            }
//        });
//
//        bahasainggris.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                SharedPreferences.Editor editor = getSharedPreferences("DATA", MODE_PRIVATE).edit();
//                editor.putString("bahasa", "2");
//                editor.commit();
//
//                toolbar = (Toolbar) findViewById(R.id.toolbar);
//                setSupportActionBar(toolbar);
//                toolbar.setTitle("Dashboard");
//
//                tabLayout.removeAllTabs();
//
//                tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.rekomen).setText("Recommendation"));
//                tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.search).setText("Search"));
//                tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.kuliner).setText("Culinary"));
//                tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.user).setText("Account"));
//
//                tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
//
//                final ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
//                if (getSharedPreferences("DATA",MODE_PRIVATE).getString("statlogin","").equals("1")){
//                    adapterlogin = new PagerAdapterHalamanRekomendasiLogin
//                            (getSupportFragmentManager(), tabLayout.getTabCount());
//                    viewPager.setAdapter(adapterlogin);
//                }
//                else{
//                    adapternologin = new PagerAdapterHalamanRekomendasiNoLogin
//                            (getSupportFragmentManager(), tabLayout.getTabCount());
//                    viewPager.setAdapter(adapternologin);
//                }
//
//                viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
//                tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
//                    @Override
//                    public void onTabSelected(TabLayout.Tab tab) {
//                        viewPager.setCurrentItem(tab.getPosition());
//                    }
//
//                    @Override
//                    public void onTabUnselected(TabLayout.Tab tab) {
//
//                    }
//
//                    @Override
//                    public void onTabReselected(TabLayout.Tab tab) {
//
//                    }
//                });
//
//            }
//        });

    }
}

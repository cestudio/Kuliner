package google.com.kuliner;

import java.util.ArrayList;
import android.app.Activity;
import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;

public class GridViewAdapter extends ArrayAdapter<GridItem> {

    private Context mContext;
    private int layoutResourceId;
    private ArrayList<GridItem> mGridData = new ArrayList<GridItem>();

    public GridViewAdapter(Context mContext, int layoutResourceId, ArrayList<GridItem> mGridData) {
        super(mContext, layoutResourceId, mGridData);
        this.layoutResourceId = layoutResourceId;
        this.mContext = mContext;
        this.mGridData = mGridData;
    }


    /**
     * Updates grid data and refresh grid items.
     * @param mGridData
     */
    public void setGridData(ArrayList<GridItem> mGridData) {
        this.mGridData = mGridData;
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ViewHolder holder;

        if (row == null) {
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            holder = new ViewHolder();
            holder.idkuliner = (TextView) row.findViewById(R.id.idkuliner);
            holder.namakuliner = (TextView) row.findViewById(R.id.kuliner);
            holder.alamat = (TextView) row.findViewById(R.id.alamat);
            holder.gambar = (ImageView) row.findViewById(R.id.gambar);
            holder.bintang1 = (ImageView) row.findViewById(R.id.bintang1);
            holder.bintang2 = (ImageView) row.findViewById(R.id.bintang2);
            holder.bintang3 = (ImageView) row.findViewById(R.id.bintang3);
            holder.bintang4 = (ImageView) row.findViewById(R.id.bintang4);
            holder.bintang5 = (ImageView) row.findViewById(R.id.bintang5);
            row.setTag(holder);
        } else {
            holder = (ViewHolder) row.getTag();
        }

        GridItem item = mGridData.get(position);
        holder.idkuliner.setText(Html.fromHtml(item.getIdkuliner()));
        holder.namakuliner.setText(Html.fromHtml(item.getNamalokasi()));
        holder.alamat.setText(Html.fromHtml(item.getAlamat()));

        Picasso.with(mContext).load(item.getGambarlokasi()).resize(500,500).into(holder.gambar);

        if (Float.valueOf(item.getRating())==0.0){
            holder.bintang1.setBackgroundResource(R.drawable.nostar);
            holder.bintang2.setBackgroundResource(R.drawable.nostar);
            holder.bintang3.setBackgroundResource(R.drawable.nostar);
            holder.bintang4.setBackgroundResource(R.drawable.nostar);
            holder.bintang5.setBackgroundResource(R.drawable.nostar);
        }
        else if (Float.valueOf(item.getRating())<=0.5){
            holder.bintang1.setBackgroundResource(R.drawable.halfstar);
            holder.bintang2.setBackgroundResource(R.drawable.nostar);
            holder.bintang3.setBackgroundResource(R.drawable.nostar);
            holder.bintang4.setBackgroundResource(R.drawable.nostar);
            holder.bintang5.setBackgroundResource(R.drawable.nostar);
        }
        else if (Float.valueOf(item.getRating())<=1.0){
            holder.bintang1.setBackgroundResource(R.drawable.fullstar);
            holder.bintang2.setBackgroundResource(R.drawable.nostar);
            holder.bintang3.setBackgroundResource(R.drawable.nostar);
            holder.bintang4.setBackgroundResource(R.drawable.nostar);
            holder.bintang5.setBackgroundResource(R.drawable.nostar);
        }
        else if (Float.valueOf(item.getRating())<=1.5){
            holder.bintang1.setBackgroundResource(R.drawable.fullstar);
            holder.bintang2.setBackgroundResource(R.drawable.halfstar);
            holder.bintang3.setBackgroundResource(R.drawable.nostar);
            holder.bintang4.setBackgroundResource(R.drawable.nostar);
            holder.bintang5.setBackgroundResource(R.drawable.nostar);
        }
        else if (Float.valueOf(item.getRating())<=2.0){
            holder.bintang1.setBackgroundResource(R.drawable.fullstar);
            holder.bintang2.setBackgroundResource(R.drawable.fullstar);
            holder.bintang3.setBackgroundResource(R.drawable.nostar);
            holder.bintang4.setBackgroundResource(R.drawable.nostar);
            holder.bintang5.setBackgroundResource(R.drawable.nostar);
        }
        else if (Float.valueOf(item.getRating())<=2.5){
            holder.bintang1.setBackgroundResource(R.drawable.fullstar);
            holder.bintang2.setBackgroundResource(R.drawable.fullstar);
            holder.bintang3.setBackgroundResource(R.drawable.halfstar);
            holder.bintang4.setBackgroundResource(R.drawable.nostar);
            holder.bintang5.setBackgroundResource(R.drawable.nostar);
        }
        else if (Float.valueOf(item.getRating())<=3.0){
            holder.bintang1.setBackgroundResource(R.drawable.fullstar);
            holder.bintang2.setBackgroundResource(R.drawable.fullstar);
            holder.bintang3.setBackgroundResource(R.drawable.fullstar);
            holder.bintang4.setBackgroundResource(R.drawable.nostar);
            holder.bintang5.setBackgroundResource(R.drawable.nostar);
        }
        else if (Float.valueOf(item.getRating())<=3.5){
            holder.bintang1.setBackgroundResource(R.drawable.fullstar);
            holder.bintang2.setBackgroundResource(R.drawable.fullstar);
            holder.bintang3.setBackgroundResource(R.drawable.fullstar);
            holder.bintang4.setBackgroundResource(R.drawable.halfstar);
            holder.bintang5.setBackgroundResource(R.drawable.nostar);
        }
        else if (Float.valueOf(item.getRating())<=4.0){
            holder.bintang1.setBackgroundResource(R.drawable.fullstar);
            holder.bintang2.setBackgroundResource(R.drawable.fullstar);
            holder.bintang3.setBackgroundResource(R.drawable.fullstar);
            holder.bintang4.setBackgroundResource(R.drawable.fullstar);
            holder.bintang5.setBackgroundResource(R.drawable.nostar);
        }
        else if (Float.valueOf(item.getRating())<=4.5){
            holder.bintang1.setBackgroundResource(R.drawable.fullstar);
            holder.bintang2.setBackgroundResource(R.drawable.fullstar);
            holder.bintang3.setBackgroundResource(R.drawable.fullstar);
            holder.bintang4.setBackgroundResource(R.drawable.fullstar);
            holder.bintang5.setBackgroundResource(R.drawable.halfstar);
        }
        else if (Float.valueOf(item.getRating())<=5.0){
            holder.bintang1.setBackgroundResource(R.drawable.fullstar);
            holder.bintang2.setBackgroundResource(R.drawable.fullstar);
            holder.bintang3.setBackgroundResource(R.drawable.fullstar);
            holder.bintang4.setBackgroundResource(R.drawable.fullstar);
            holder.bintang5.setBackgroundResource(R.drawable.fullstar);
        }


        return row;
    }

    static class ViewHolder {
        TextView namakuliner,idkuliner,alamat;
        ImageView gambar,bintang1,bintang2,bintang3,bintang4,bintang5;
    }
}
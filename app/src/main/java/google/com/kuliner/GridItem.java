package google.com.kuliner;


public class GridItem {

    String namamakanan,gambarlokasi,gambarkuliner,kuliner,rating,idlokasi,idkuliner,alamat,namalokasi;

    public String getGambarlokasi() {

        return gambarlokasi;

    }

    public void setGambarlokasi(String gambarlokasi) {

        this.gambarlokasi = gambarlokasi;

    }
    public String getGambarkuliner() {

        return gambarkuliner;

    }

    public void setGambarkuliner(String gambarkuliner) {

        this.gambarkuliner = gambarkuliner;

    }

    public String getKuliner() {

        return kuliner;

    }

    public void setKuliner(String kuliner) {

        this.kuliner = kuliner;

    }

    public String getRating() {

        return rating;

    }

    public void setRating(String rating) {

        this.rating = rating;

    }

    public String getIdlokasi() {

        return idlokasi;

    }

    public void setIdlokasi(String idlokasi) {

        this.idlokasi = idlokasi;

    }

    public String getIdkuliner() {

        return idkuliner;

    }

    public void setIdkuliner(String idkuliner) {

        this.idkuliner = idkuliner;

    }

    public String getAlamat() {

        return alamat;

    }

    public void setAlamat(String alamat) {

        this.alamat = alamat;

    }

    public String getNamalokasi() {

        return namalokasi;

    }

    public void setNamalokasi(String namalokasi) {

        this.namalokasi = namalokasi;

    }

    public String getNamamakanan() {

        return namamakanan;

    }

    public void setNamamakanan(String namamakanan) {

        this.namamakanan = namamakanan;

    }
}
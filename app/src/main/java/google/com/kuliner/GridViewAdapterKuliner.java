package google.com.kuliner;

import android.app.Activity;
import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class GridViewAdapterKuliner extends ArrayAdapter<GridItem> {

    private Context mContext;
    private int layoutResourceId;
    private ArrayList<GridItem> mGridData = new ArrayList<GridItem>();

    public GridViewAdapterKuliner(Context mContext, int layoutResourceId, ArrayList<GridItem> mGridData) {
        super(mContext, layoutResourceId, mGridData);
        this.layoutResourceId = layoutResourceId;
        this.mContext = mContext;
        this.mGridData = mGridData;
    }


    /**
     * Updates grid data and refresh grid items.
     * @param mGridData
     */
    public void setGridData(ArrayList<GridItem> mGridData) {
        this.mGridData = mGridData;
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ViewHolder holder;

        if (row == null) {
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            holder = new ViewHolder();
            holder.idkuliner = (TextView) row.findViewById(R.id.idkuliner);
            holder.namakuliner = (TextView) row.findViewById(R.id.kuliner);
            holder.alamat = (TextView) row.findViewById(R.id.alamat);
            holder.gambar = (ImageView) row.findViewById(R.id.gambar);
            row.setTag(holder);
        } else {
            holder = (ViewHolder) row.getTag();
        }

        GridItem item = mGridData.get(position);
        holder.idkuliner.setText(Html.fromHtml(item.getIdkuliner()));
        holder.namakuliner.setText(Html.fromHtml(item.getKuliner()));
//        holder.alamat.setText(Html.fromHtml(item.getAlamat()));

        Picasso.with(mContext).load(item.getGambarlokasi()).resize(500,500).into(holder.gambar);

        return row;
    }

    static class ViewHolder {
        TextView namakuliner,idkuliner,alamat;
        ImageView gambar,bintang1,bintang2,bintang3,bintang4,bintang5;
    }
}
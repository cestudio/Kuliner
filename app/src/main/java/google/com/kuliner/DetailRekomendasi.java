package google.com.kuliner;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TableLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import android.support.v7.app.ActionBarActivity;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;

public class DetailRekomendasi extends ActionBarActivity {

    ExpandableHeightListView listkomentar;

    public static String FEED_URL;

    TextView forum,namakuliner,namalokasi,alamatkuliner,jambuka,menu,note,deskripsi,detailnilairating,detailulasan,detailnamapengguna,detailperasaan,detailkirim;
    ImageView imggambar,bintang1,bintang2,bintang3,bintang4,bintang5,nilaibintang1,nilaibintang2,nilaibintang3,nilaibintang4,nilaibintang5;
    LinearLayout detailbtntelepon,detailbtnpeta,detaillayoutnilairating,detailbtnlistmakanan;
    TableLayout layoutphonemap,layoutdetail;

    public static Activity fa;

    String id;

    private static final String TAG_RESULT = "result";
    private static final String TAG_JAMBUKA = "jambuka";
    private static final String TAG_MENU = "menu";
    private static final String TAG_TELEPON = "telepon";
    private static final String TAG_CACATAN= "catatan";
    private static final String TAG_DISKRIPSI= "deskripsi";
    private static final String TAG_RATING = "rating";
    private static final String TAG_STATUSRATING = "statusrating";
    private static final String TAG_JUMRATING = "jumrating";
    private static final String TAG_NILAIRATING = "nilairating";
    private static final String TAG_NAMAUSER = "namauser";

    static boolean a=false;

    JSONArray rs = null;

    String myJSON;

    private static final String TAG_USERNAMA = "nama";
    private static final String TAG_USERNILAIRATING = "nilairating";
    private static final String TAG_USERWAKTU = "waktu";
    private static final String TAG_USERKOMENTAR = "komentar";

    JSONArray data = null;

    ArrayList<HashMap<String, String>> komentarList;

    String strtelepon;
    String strstatusrating;

    int usernilairating;

    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        fa=this;

        if (getSharedPreferences("DATA", Context.MODE_PRIVATE).getString("bahasa","").equals("2")){
            setContentView(R.layout.activity_detail_rekomendasi_inggris);
            toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);

            toolbar.setTitle("Recomendation Culinary");
        }
        else{
            setContentView(R.layout.activity_detail_rekomendasi);
            toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);

            toolbar.setTitle("Detail Rekomendasi Kuliner");
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        komentarList = new ArrayList<HashMap<String, String>>();

        id = getSharedPreferences("DATA",MODE_PRIVATE).getString("idkuliner","");
        String kuliner = getSharedPreferences("DATA",MODE_PRIVATE).getString("kuliner","");
        String lokasi = getSharedPreferences("DATA",MODE_PRIVATE).getString("lokasi","");
        String alamat = getSharedPreferences("DATA",MODE_PRIVATE).getString("alamat","");
        String gambar = getSharedPreferences("DATA",MODE_PRIVATE).getString("gambar","");
        listkomentar = (ExpandableHeightListView) findViewById(R.id.listkomentar);
        listkomentar.setExpanded(true);

        detailbtntelepon = (LinearLayout) findViewById(R.id.detailbtntelepon);
        detailbtnpeta = (LinearLayout) findViewById(R.id.detailbtnpeta);
        detaillayoutnilairating = (LinearLayout) findViewById(R.id.detaillayoutnilairating);
        detailbtnlistmakanan = (LinearLayout) findViewById(R.id.detailbtnlistmakanan);

        layoutphonemap = (TableLayout) findViewById(R.id.layoutphonemap);
        layoutdetail = (TableLayout) findViewById(R.id.layoutdetail);

        detailnamapengguna = (TextView) findViewById(R.id.detailnamapengguna);
        detailnilairating = (TextView) findViewById(R.id.detailnilairating);
        detailulasan = (TextView) findViewById(R.id.detailulasan);
        detailperasaan = (TextView) findViewById(R.id.detailperasaan);
        detailkirim = (TextView) findViewById(R.id.detailkirim);
        bintang1 = (ImageView) findViewById(R.id.detailbintang1);
        bintang2 = (ImageView) findViewById(R.id.detailbintang2);
        bintang3 = (ImageView) findViewById(R.id.detailbintang3);
        bintang4 = (ImageView) findViewById(R.id.detailbintang4);
        bintang5 = (ImageView) findViewById(R.id.detailbintang5);
        nilaibintang1 = (ImageView) findViewById(R.id.detailnilaibintang1);
        nilaibintang2 = (ImageView) findViewById(R.id.detailnilaibintang2);
        nilaibintang3 = (ImageView) findViewById(R.id.detailnilaibintang3);
        nilaibintang4 = (ImageView) findViewById(R.id.detailnilaibintang4);
        nilaibintang5 = (ImageView) findViewById(R.id.detailnilaibintang5);

        forum = (TextView) findViewById(R.id.textforum);
        namalokasi = (TextView) findViewById(R.id.detailnamalokasi);
        namakuliner = (TextView) findViewById(R.id.detailnamakuliner);
        alamatkuliner = (TextView) findViewById(R.id.detaillokasi);
        imggambar = (ImageView) findViewById(R.id.detailgambar);
        jambuka = (TextView) findViewById(R.id.detailjambuka);
        menu = (TextView) findViewById(R.id.detailmenu);
        note = (TextView) findViewById(R.id.detailnote);
        deskripsi = (TextView) findViewById(R.id.detaildeskripsi);

        note.setVisibility(View.GONE);

        menu.setVisibility(View.GONE);
        namakuliner.setVisibility(View.GONE);

        namalokasi.setText(lokasi);
        namakuliner.setText(kuliner);
        alamatkuliner.setText(alamat);

        Picasso.with(this).load(gambar).resize(500,500).into(imggambar);

        detailbtntelepon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + strtelepon));
                startActivity(i);
            }
        });

        detailbtnpeta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(DetailRekomendasi.this,Peta.class));
            }
        });

        detailbtnlistmakanan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(DetailRekomendasi.this,ListMakanan.class));
            }
        });

        nilaibintang1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                usernilairating = 1;
                detailperasaan.setText("Benci");
                nilaibintang1.setBackgroundResource(R.drawable.fullstar);
                nilaibintang2.setBackgroundResource(R.drawable.nostar);
                nilaibintang3.setBackgroundResource(R.drawable.nostar);
                nilaibintang4.setBackgroundResource(R.drawable.nostar);
                nilaibintang5.setBackgroundResource(R.drawable.nostar);
            }
        });

        nilaibintang2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                usernilairating = 2;
                detailperasaan.setText("Tidak suka");
                nilaibintang1.setBackgroundResource(R.drawable.fullstar);
                nilaibintang2.setBackgroundResource(R.drawable.fullstar);
                nilaibintang3.setBackgroundResource(R.drawable.nostar);
                nilaibintang4.setBackgroundResource(R.drawable.nostar);
                nilaibintang5.setBackgroundResource(R.drawable.nostar);
            }
        });

        nilaibintang3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                usernilairating = 3;
                detailperasaan.setText("Lumayan");
                nilaibintang1.setBackgroundResource(R.drawable.fullstar);
                nilaibintang2.setBackgroundResource(R.drawable.fullstar);
                nilaibintang3.setBackgroundResource(R.drawable.fullstar);
                nilaibintang4.setBackgroundResource(R.drawable.nostar);
                nilaibintang5.setBackgroundResource(R.drawable.nostar);
            }
        });

        nilaibintang4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                usernilairating = 4;
                detailperasaan.setText("Menyukai ini");
                nilaibintang1.setBackgroundResource(R.drawable.fullstar);
                nilaibintang2.setBackgroundResource(R.drawable.fullstar);
                nilaibintang3.setBackgroundResource(R.drawable.fullstar);
                nilaibintang4.setBackgroundResource(R.drawable.fullstar);
                nilaibintang5.setBackgroundResource(R.drawable.nostar);
            }
        });

        nilaibintang5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                usernilairating = 5;
                detailperasaan.setText("Suka sekali");
                nilaibintang1.setBackgroundResource(R.drawable.fullstar);
                nilaibintang2.setBackgroundResource(R.drawable.fullstar);
                nilaibintang3.setBackgroundResource(R.drawable.fullstar);
                nilaibintang4.setBackgroundResource(R.drawable.fullstar);
                nilaibintang5.setBackgroundResource(R.drawable.fullstar);
            }
        });

        detailkirim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = getSharedPreferences("DATA", MODE_PRIVATE).edit();
                editor.putString("nilairating", String.valueOf(usernilairating));
                editor.commit();

                startActivity(new Intent(DetailRekomendasi.this,ListMakanan.class));
                finish();
            }
        });

        new Detail().execute();
        ListKomentar();
    }

    //JSON parser, mengambil data dari web service,
    private class Detail extends AsyncTask<String, String, JSONObject> {
        @Override
        protected void onPreExecute(){

        }
        //menjalankan proses di background, tidak mengganggu proses lain
        @Override
        protected JSONObject doInBackground(String... args)
        {
            FEED_URL = "http://";
            FEED_URL += getSharedPreferences("DATA",MODE_PRIVATE).getString("IP","");
            FEED_URL += Config.DETAIL_URL;
            String URL = FEED_URL;
            if (getSharedPreferences("DATA",MODE_PRIVATE).getString("statlogin","").equals("1")){
                URL += "?idmakanan="+id;
//            URL += "&iduser=1";
                URL += "&iduser="+getSharedPreferences("DATA",MODE_PRIVATE).getString("iduser","");
                URL += "&idlokasi="+getSharedPreferences("DATA",MODE_PRIVATE).getString("idlokasi","");
                URL += "&detail="+getSharedPreferences("DATA",MODE_PRIVATE).getString("detail","");
            }
            else{
                URL += "?idmakanan="+id;
//            URL += "&iduser=1";
                URL += "&iduser=0";
                URL += "&idlokasi="+getSharedPreferences("DATA",MODE_PRIVATE).getString("idlokasi","");
                URL += "&detail="+getSharedPreferences("DATA",MODE_PRIVATE).getString("detail","");
            }

            Log.e("URL : ",URL);

            //Membuat JSON Parser instance
            JSONParser jParser = new JSONParser();

            //mengambil JSON String dari url
            JSONObject json = jParser.getJSONFromUrl(URL);
            if(json==null)
            {
                a=false;
            }
            else a=true;
            return json;
        }
        protected void onPostExecute(JSONObject json) {
            if(a==true)
            {
                try{
                    Log.e("status",a+"");
                    //mengambil array rumah sakit
                    //mengambil array rumah sakit
                    rs = json.getJSONArray(TAG_RESULT);
                    //loop pada rumah sakit
                    for(int i=0; i<rs.length();i++)
                    {
                        JSONObject a = rs.getJSONObject(i);
                        String strjambuka = a.getString(TAG_JAMBUKA);
                        strtelepon = a.getString(TAG_TELEPON);
                        String strcatatan = a.getString(TAG_CACATAN);
                        String strdiskripsi = a.getString(TAG_DISKRIPSI);
                        String strrating = a.getString(TAG_RATING);
                        strstatusrating = a.getString(TAG_STATUSRATING);
                        String strjumrating = a.getString(TAG_JUMRATING);
                        String strnilairating = a.getString(TAG_NILAIRATING);
                        String strnamauser = a.getString(TAG_NAMAUSER);

                        jambuka.setText(strjambuka);
                        note.setText(strcatatan);
                        deskripsi.setText(strcatatan);
                        detailnilairating.setText(strrating);

                        if (getSharedPreferences("DATA", Context.MODE_PRIVATE).getString("bahasa","").equals("2")){
                            detailulasan.setText(strjumrating+" Forum");
                        }
                        else{
                            detailulasan.setText(strjumrating+" Ulasan");
                        }

                        detailnamapengguna.setText(strnamauser);

                        if(strnamauser.equals("Guest")){
                            detaillayoutnilairating.setVisibility(View.GONE);
                        }
                        else{
                            if(strstatusrating.equals("1")){
                                detaillayoutnilairating.setVisibility(View.GONE);
                            }
                            else{
                                detaillayoutnilairating.setVisibility(View.VISIBLE);
                            }
                        }

                        if (Float.valueOf(strrating)==0.0){
                            bintang1.setBackgroundResource(R.drawable.nostar);
                            bintang2.setBackgroundResource(R.drawable.nostar);
                            bintang3.setBackgroundResource(R.drawable.nostar);
                            bintang4.setBackgroundResource(R.drawable.nostar);
                            bintang5.setBackgroundResource(R.drawable.nostar);
                        }
                        else if (Float.valueOf(strrating)<=0.5){
                            bintang1.setBackgroundResource(R.drawable.halfstar);
                            bintang2.setBackgroundResource(R.drawable.nostar);
                            bintang3.setBackgroundResource(R.drawable.nostar);
                            bintang4.setBackgroundResource(R.drawable.nostar);
                            bintang5.setBackgroundResource(R.drawable.nostar);
                        }
                        else if (Float.valueOf(strrating)<=1.0){
                            bintang1.setBackgroundResource(R.drawable.fullstar);
                            bintang2.setBackgroundResource(R.drawable.nostar);
                            bintang3.setBackgroundResource(R.drawable.nostar);
                            bintang4.setBackgroundResource(R.drawable.nostar);
                            bintang5.setBackgroundResource(R.drawable.nostar);
                        }
                        else if (Float.valueOf(strrating)<=1.5){
                            bintang1.setBackgroundResource(R.drawable.fullstar);
                            bintang2.setBackgroundResource(R.drawable.halfstar);
                            bintang3.setBackgroundResource(R.drawable.nostar);
                            bintang4.setBackgroundResource(R.drawable.nostar);
                            bintang5.setBackgroundResource(R.drawable.nostar);
                        }
                        else if (Float.valueOf(strrating)<=2.0){
                            bintang1.setBackgroundResource(R.drawable.fullstar);
                            bintang2.setBackgroundResource(R.drawable.fullstar);
                            bintang3.setBackgroundResource(R.drawable.nostar);
                            bintang4.setBackgroundResource(R.drawable.nostar);
                            bintang5.setBackgroundResource(R.drawable.nostar);
                        }
                        else if (Float.valueOf(strrating)<=2.5){
                            bintang1.setBackgroundResource(R.drawable.fullstar);
                            bintang2.setBackgroundResource(R.drawable.fullstar);
                            bintang3.setBackgroundResource(R.drawable.halfstar);
                            bintang4.setBackgroundResource(R.drawable.nostar);
                            bintang5.setBackgroundResource(R.drawable.nostar);
                        }
                        else if (Float.valueOf(strrating)<=3.0){
                            bintang1.setBackgroundResource(R.drawable.fullstar);
                            bintang2.setBackgroundResource(R.drawable.fullstar);
                            bintang3.setBackgroundResource(R.drawable.fullstar);
                            bintang4.setBackgroundResource(R.drawable.nostar);
                            bintang5.setBackgroundResource(R.drawable.nostar);
                        }
                        else if (Float.valueOf(strrating)<=3.5){
                            bintang1.setBackgroundResource(R.drawable.fullstar);
                            bintang2.setBackgroundResource(R.drawable.fullstar);
                            bintang3.setBackgroundResource(R.drawable.fullstar);
                            bintang4.setBackgroundResource(R.drawable.halfstar);
                            bintang5.setBackgroundResource(R.drawable.nostar);
                        }
                        else if (Float.valueOf(strrating)<=4.0){
                            bintang1.setBackgroundResource(R.drawable.fullstar);
                            bintang2.setBackgroundResource(R.drawable.fullstar);
                            bintang3.setBackgroundResource(R.drawable.fullstar);
                            bintang4.setBackgroundResource(R.drawable.fullstar);
                            bintang5.setBackgroundResource(R.drawable.nostar);
                        }
                        else if (Float.valueOf(strrating)<=4.5){
                            bintang1.setBackgroundResource(R.drawable.fullstar);
                            bintang2.setBackgroundResource(R.drawable.fullstar);
                            bintang3.setBackgroundResource(R.drawable.fullstar);
                            bintang4.setBackgroundResource(R.drawable.fullstar);
                            bintang5.setBackgroundResource(R.drawable.halfstar);
                        }
                        else if (Float.valueOf(strrating)<=5.0){
                            bintang1.setBackgroundResource(R.drawable.fullstar);
                            bintang2.setBackgroundResource(R.drawable.fullstar);
                            bintang3.setBackgroundResource(R.drawable.fullstar);
                            bintang4.setBackgroundResource(R.drawable.fullstar);
                            bintang5.setBackgroundResource(R.drawable.fullstar);
                        }

                        if (Float.valueOf(strnilairating)==0.0){
                            nilaibintang1.setBackgroundResource(R.drawable.nostar);
                            nilaibintang2.setBackgroundResource(R.drawable.nostar);
                            nilaibintang3.setBackgroundResource(R.drawable.nostar);
                            nilaibintang4.setBackgroundResource(R.drawable.nostar);
                            nilaibintang5.setBackgroundResource(R.drawable.nostar);
                        }
                        else if (Float.valueOf(strnilairating)<=0.5){
                            nilaibintang1.setBackgroundResource(R.drawable.halfstar);
                            nilaibintang2.setBackgroundResource(R.drawable.nostar);
                            nilaibintang3.setBackgroundResource(R.drawable.nostar);
                            nilaibintang4.setBackgroundResource(R.drawable.nostar);
                            nilaibintang5.setBackgroundResource(R.drawable.nostar);
                        }
                        else if (Float.valueOf(strnilairating)<=1.0){
                            nilaibintang1.setBackgroundResource(R.drawable.fullstar);
                            nilaibintang2.setBackgroundResource(R.drawable.nostar);
                            nilaibintang3.setBackgroundResource(R.drawable.nostar);
                            nilaibintang4.setBackgroundResource(R.drawable.nostar);
                            nilaibintang5.setBackgroundResource(R.drawable.nostar);
                        }
                        else if (Float.valueOf(strnilairating)<=1.5){
                            nilaibintang1.setBackgroundResource(R.drawable.fullstar);
                            nilaibintang2.setBackgroundResource(R.drawable.halfstar);
                            nilaibintang3.setBackgroundResource(R.drawable.nostar);
                            nilaibintang4.setBackgroundResource(R.drawable.nostar);
                            nilaibintang5.setBackgroundResource(R.drawable.nostar);
                        }
                        else if (Float.valueOf(strnilairating)<=2.0){
                            nilaibintang1.setBackgroundResource(R.drawable.fullstar);
                            nilaibintang2.setBackgroundResource(R.drawable.fullstar);
                            nilaibintang3.setBackgroundResource(R.drawable.nostar);
                            nilaibintang4.setBackgroundResource(R.drawable.nostar);
                            nilaibintang5.setBackgroundResource(R.drawable.nostar);
                        }
                        else if (Float.valueOf(strnilairating)<=2.5){
                            nilaibintang1.setBackgroundResource(R.drawable.fullstar);
                            nilaibintang2.setBackgroundResource(R.drawable.fullstar);
                            nilaibintang3.setBackgroundResource(R.drawable.halfstar);
                            nilaibintang4.setBackgroundResource(R.drawable.nostar);
                            nilaibintang5.setBackgroundResource(R.drawable.nostar);
                        }
                        else if (Float.valueOf(strnilairating)<=3.0){
                            nilaibintang1.setBackgroundResource(R.drawable.fullstar);
                            nilaibintang2.setBackgroundResource(R.drawable.fullstar);
                            nilaibintang3.setBackgroundResource(R.drawable.fullstar);
                            nilaibintang4.setBackgroundResource(R.drawable.nostar);
                            nilaibintang5.setBackgroundResource(R.drawable.nostar);
                        }
                        else if (Float.valueOf(strnilairating)<=3.5){
                            nilaibintang1.setBackgroundResource(R.drawable.fullstar);
                            nilaibintang2.setBackgroundResource(R.drawable.fullstar);
                            nilaibintang3.setBackgroundResource(R.drawable.fullstar);
                            nilaibintang4.setBackgroundResource(R.drawable.halfstar);
                            nilaibintang5.setBackgroundResource(R.drawable.nostar);
                        }
                        else if (Float.valueOf(strnilairating)<=4.0){
                            nilaibintang1.setBackgroundResource(R.drawable.fullstar);
                            nilaibintang2.setBackgroundResource(R.drawable.fullstar);
                            nilaibintang3.setBackgroundResource(R.drawable.fullstar);
                            nilaibintang4.setBackgroundResource(R.drawable.fullstar);
                            nilaibintang5.setBackgroundResource(R.drawable.nostar);
                        }
                        else if (Float.valueOf(strnilairating)<=4.5){
                            nilaibintang1.setBackgroundResource(R.drawable.fullstar);
                            nilaibintang2.setBackgroundResource(R.drawable.fullstar);
                            nilaibintang3.setBackgroundResource(R.drawable.fullstar);
                            nilaibintang4.setBackgroundResource(R.drawable.fullstar);
                            nilaibintang5.setBackgroundResource(R.drawable.halfstar);
                        }
                        else if (Float.valueOf(strnilairating)<=5.0){
                            nilaibintang1.setBackgroundResource(R.drawable.fullstar);
                            nilaibintang2.setBackgroundResource(R.drawable.fullstar);
                            nilaibintang3.setBackgroundResource(R.drawable.fullstar);
                            nilaibintang4.setBackgroundResource(R.drawable.fullstar);
                            nilaibintang5.setBackgroundResource(R.drawable.fullstar);
                        }

                    }
                }catch(JSONException e)
                {
                    e.printStackTrace();
                }
            }
            //Jika tidak ada koneksi atau server down, keluarkan message error
            else {

                Toast.makeText(getApplicationContext(), "Server Maintenance", Toast.LENGTH_SHORT).show();
                Log.e("status",a+"");}
        }

    }

    protected void showList(){
        try {
            JSONObject jsonObj = new JSONObject(myJSON);
            data = jsonObj.getJSONArray(TAG_RESULT);

            for(int i=0;i<data.length();i++){
                JSONObject c = data.getJSONObject(i);
                String nama = c.getString(TAG_USERNAMA);
                String nilairating = c.getString(TAG_USERNILAIRATING);
                String waktu = c.getString(TAG_USERWAKTU);
                String komentar = c.getString(TAG_USERKOMENTAR);

                if (Float.valueOf(nilairating)==0.0){
                    nilairating = "✩✩✩✩✩";
                }
                else if (Float.valueOf(nilairating)<=1.0){
                    nilairating = "★✩✩✩✩";
                }
                else if (Float.valueOf(nilairating)<=2.0){
                    nilairating = "★★✩✩✩";
                }
                else if (Float.valueOf(nilairating)<=3.0){
                    nilairating = "★★★✩✩";
                }
                else if (Float.valueOf(nilairating)<=4.0){
                    nilairating = "★★★★✩";
                }
                else if (Float.valueOf(nilairating)<=5.0){
                    nilairating = "★★★★★";
                }

                HashMap<String, String> persons = new HashMap<String, String>();;

                persons.put(TAG_USERNAMA,nama);
                persons.put(TAG_USERNILAIRATING,nilairating);
                persons.put(TAG_USERWAKTU,waktu);
                persons.put(TAG_USERKOMENTAR,komentar);

                komentarList.add(persons);
            }

            ListAdapter adapter = new SimpleAdapter(
                    this, komentarList, R.layout.list_komentar,
                    new String[]{TAG_USERNAMA,TAG_USERNILAIRATING,TAG_USERWAKTU,TAG_USERKOMENTAR},
                    new int[]{R.id.komentarnama,R.id.komentarnilaibintang,R.id.komentarwaktu,R.id.komentar}
            );
            listkomentar.setAdapter(adapter);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void ListKomentar(){
        class GetDataJSON extends AsyncTask<String, Void, String> {

            @Override
            protected String doInBackground(String... params) {
                try {

                    String link;
                    link = "http://";
                    link += getSharedPreferences("DATA",MODE_PRIVATE).getString("IP","");
                    link += Config.LISTKOMETARDETAILHALAMANREKOMENDASI_URL;
                    link += "?idmakanan="+getSharedPreferences("DATA",Context.MODE_PRIVATE).getString("idkuliner","");
                    link += "&idlokasi="+getSharedPreferences("DATA",Context.MODE_PRIVATE).getString("idlokasi","");

                    URL url = new URL(link);
                    URLConnection conn = url.openConnection();

                    conn.setDoOutput(true);
                    OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());

                    wr.flush();

                    BufferedReader reader = new BufferedReader(new
                            InputStreamReader(conn.getInputStream()));

                    StringBuilder sb = new StringBuilder();
                    String line = null;

                    // Read Server Response
                    while ((line = reader.readLine()) != null) {
                        sb.append(line);
                        break;
                    }

                    return sb.toString();
                } catch (Exception e) {
                    return new String("Exception: " + e.getMessage());
                }
            }
            @Override
            protected void onPostExecute (String result){
                myJSON=result;
                Log.e("Data =",result);
                showList();
            }
        }
        GetDataJSON g = new GetDataJSON();
        g.execute();
    }


}
package google.com.kuliner;

import android.app.Activity;
import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class GridViewAdapterListLokasiKuliner extends ArrayAdapter<GridItemListLokasiKuliner> {

    private Context mContext;
    private int layoutResourceId;
    private ArrayList<GridItemListLokasiKuliner> mGridData = new ArrayList<GridItemListLokasiKuliner>();

    public GridViewAdapterListLokasiKuliner(Context mContext, int layoutResourceId, ArrayList<GridItemListLokasiKuliner> mGridData) {
        super(mContext, layoutResourceId, mGridData);
        this.layoutResourceId = layoutResourceId;
        this.mContext = mContext;
        this.mGridData = mGridData;
    }


    /**
     * Updates grid data and refresh grid items.
     * @param mGridData
     */
    public void setGridData(ArrayList<GridItemListLokasiKuliner> mGridData) {
        this.mGridData = mGridData;
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ViewHolder holder;

        if (row == null) {
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            holder = new ViewHolder();
            holder.idlokasi = (TextView) row.findViewById(R.id.idlokasi);
            holder.namalokasi = (TextView) row.findViewById(R.id.namalokasi);
            holder.gambar = (ImageView) row.findViewById(R.id.gambar);
            row.setTag(holder);
        } else {
            holder = (ViewHolder) row.getTag();
        }

        GridItemListLokasiKuliner item = mGridData.get(position);
        holder.idlokasi.setText(Html.fromHtml(item.getIdlokasi()));
        holder.namalokasi.setText(Html.fromHtml(item.getNamalokasi()));

        Picasso.with(mContext).load(item.getGambar()).resize(500,500).into(holder.gambar);

        return row;
    }

    static class ViewHolder {
        TextView namalokasi,idlokasi;
        ImageView gambar;
    }
}